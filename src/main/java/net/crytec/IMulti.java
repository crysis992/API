package net.crytec;

import net.crytec.api.redis.RedisManager.ServerType;

/**
 * Used for Bungee/Spigot compatibility
 * 
 * @author crysis992
 */
public interface IMulti {

	/**
	 * Runs a Runnable one time.
	 * 
	 * @param runnable
	 *            the Runnable to run.
	 */
	public void runAsync(Runnable runnable);

	/**
	 * The {@link ServerType} this implementation is running at.
	 * 
	 * @return the current ServerType
	 */
	public default ServerType getServerType() {
		return ServerType.SPIGOT;
	}
}