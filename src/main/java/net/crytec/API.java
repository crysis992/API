package net.crytec;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import co.aikar.commands.BukkitCommandManager;
import co.aikar.commands.InvalidCommandArgument;
import lombok.Getter;
import net.crytec.api.Metrics;
import net.crytec.api.Versions;
import net.crytec.api.NoteBlockAPI.NoteBlockPlayerManager;
import net.crytec.api.WGRegionEvents.WGRegionEventsListener;
import net.crytec.api.actionbar.ActionBarHandler;
import net.crytec.api.blockrestore.BlockRestore;
import net.crytec.api.chat.chatpause.ChatQueueManager;
import net.crytec.api.chat.program.ChatEditorCore;
import net.crytec.api.config.playerdata.PlayerDataManager;
import net.crytec.api.events.PluginLoadTracker;
import net.crytec.api.itemstack.customitems.CustomItem;
import net.crytec.api.itemstack.customitems.CustomItemManager;
import net.crytec.api.listener.GlobalListener;
import net.crytec.api.nbt.NBTTester;
import net.crytec.api.persistentblocks.PersistentBlockAPI;
import net.crytec.api.persistentblocks.PersistentBlockManager;
import net.crytec.api.recharge.Recharge;
import net.crytec.api.redis.RedisManager;
import net.crytec.api.redis.ServernameRequestPacket;
import net.crytec.api.scoreboard.BoardManager;
import net.crytec.api.smartInv.InventoryManager;
import net.crytec.api.spigetupdater.SpigetUpdate;
import net.crytec.api.spigetupdater.core.UpdateCallback;
import net.crytec.api.util.ChatStringInput;
import net.crytec.api.util.F;
import net.crytec.api.util.language.LanguageHelper;
import net.crytec.api.util.language.convert.EnumLang;
import net.milkbowl.vault.economy.Economy;
/**
 * 
 * Main class, JavaPlugin implementation of this API
 * conainting mostly but not exclusively Spigot related
 * utility classes.
 * 
 * @author crysis992
 * @author Gestankbratwurst
 *
 */
public class API extends JavaPlugin implements IMulti {
	
	/**
	 * Singleton of this plugin.
	 */
	@Getter
	private static API instance;
		
	/**
	 * Used to start, stop and control volume of songs.
	 */
	@Getter
	private NoteBlockPlayerManager noteBlockPlayerManager;
	
	/**
	 * Manager for Redis related operations.
	 */
	@Getter
	private RedisManager redisManager;
	
	/**
	 * Used to stop Messages for certain players, then
	 * send every missed message after leaving the queue.
	 */
	@Getter
	private ChatQueueManager chatQueueManager;
	
	/**
	 * Manager for Json formatted chat programms.
	 */
	@Getter
	private ChatEditorCore chatProgramAPI;
	
	/**
	 * Manager for items with action events.
	 */
	@Getter
	private CustomItemManager customItemFactory;
	
	private PersistentBlockAPI persistentHandler;
	
	private Economy econ;
	private boolean econEnabled;
	
	/**
	 * Entrypoint for the PersistentBlockAPI.
	 */
	@Getter
	private PersistentBlockManager persistentBlocks;
	
	private final HashMap<Hooks, Boolean> hooks = Maps.newHashMap();
	
	@Override
	public void onLoad() {
		API.instance = this;
		this.getDataFolder().mkdirs();
		Versions.initialize();
	}

	@Override
	public void onEnable() {
		this.logToConsole(this, "§8Loading API version §9" + this.getDescription().getVersion());
		
		new PluginLoadTracker(this);
		
		if (!new File(getDataFolder(), "config.yml").exists()) {
			this.saveResource("config.yml", false);
		}
		
		this.econEnabled = this.setupEconomy();
				
		String MAIN = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.main", "&9%module%&7> "));
		String ERROR = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.error", "&4Fehler&7> "));
		String NAME = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.name", "&6%var%&7"));
		String ELEM = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.element", "&d%var%&7"));
		String ON = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.on", "&2On"));
		String OFF = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.off", "&cOff"));
		String YES = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.yes", "&2Yes"));
		String NO = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.no", "&2No"));

		F.init(MAIN, ERROR, NAME, ELEM, ON, OFF, YES, NO);
		
		new InventoryManager();
		new PlayerDataManager();
		new ActionBarHandler();

		ChatStringInput.initialize();
		BlockRestore.initialize();
		new Recharge(this);
		
		this.noteBlockPlayerManager = new NoteBlockPlayerManager();
		this.persistentBlocks = new PersistentBlockManager(this);
		this.persistentHandler = new PersistentBlockAPI(this, this.persistentBlocks);
		this.customItemFactory = new CustomItemManager(this);
		
		Bukkit.getPluginManager().registerEvents(new GlobalListener(), this);
		
		// Setup Language 
		
		String language = API.getInstance().getConfig().getString("languageAPI", "EN_US");
		EnumLang lang = EnumLang.EN_US;
		
		if (!EnumUtils.isValidEnum(EnumLang.class, language)) {
			this.logToConsole(this, "§4Failed to load language: " + language + " - Invalid language code");
			this.logToConsole(this, "§4Valid language codes:");
			API.getInstance().getLogger().severe(Arrays.stream(EnumLang.values()).map(EnumLang::toString).collect(Collectors.joining(", ")));
		} else {
			lang = EnumLang.valueOf(language);
		}
		LanguageHelper.DEFAULT_LANGUAGE = lang;
		EnumLang.init(lang);

		// Initialize dependency apis
		
		if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
			this.chatQueueManager = new ChatQueueManager(this);
			this.chatProgramAPI = new ChatEditorCore(this);
			this.hooks.put(Hooks.PROTOCOLLIB, true);
		} else {
			this.hooks.put(Hooks.PROTOCOLLIB, false);
		}
		
		if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
			if (!getConfig().getBoolean("modules.WorldGuard", true)) {
				this.hooks.put(Hooks.WORLDGUARD, false);
			} else {

				String version = Bukkit.getPluginManager().getPlugin("WorldGuard").getDescription().getVersion();
				if (!version.startsWith("7")) {
					this.logToConsole(this, "§4WorldGuard Events require WorldGuard 7 or higher");
					this.hooks.put(Hooks.WORLDGUARD, false);
				} else {

					try {
						Class.forName("com.sk89q.worldedit.math.Vector3"); 
						WGRegionEventsListener.initialize();
						this.hooks.put(Hooks.WORLDGUARD, true);
					} catch (ClassNotFoundException e) {
						this.logToConsole(this, "§4WorldGuard Events require the latest version of WorldGuard 7 and WorldEdit 7");
						this.hooks.put(Hooks.WORLDGUARD, false);
					}
				}
			}
		} else {
			this.hooks.put(Hooks.WORLDGUARD, false);
		}
				
		NBTTester nbttest = new NBTTester();
		if (nbttest.isCompatible()) {
			this.hooks.put(Hooks.NBT, true);
		} else {
			this.hooks.put(Hooks.NBT, false);
		}
		
		if (getConfig().getBoolean("useRedis", false)) {
		this.redisManager = new RedisManager(this);
		this.redisManager.registerProvider(new ServernameRequestPacket(this.redisManager));
    	this.redisManager.requestServername();
		this.hooks.put(Hooks.REDIS, true);
		} else {
			this.hooks.put(Hooks.REDIS, false);
		}
		
		if (getConfig().getBoolean("modules.scoreboard", false)) {
			if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
				BoardManager.get();
				this.hooks.put(Hooks.SCOREBOARDAPI, true);
			} else {
				this.logToConsole(this, "§4Unable to initialize ScoreboardAPI - ProtocolLib is not installed.");
				this.hooks.put(Hooks.SCOREBOARDAPI, false);
			}
		} else {
			this.hooks.put(Hooks.SCOREBOARDAPI, false);
		}
		
		this.broadcastHooks();
		this.logToConsole(this, "§aServer Fork: §8" + Versions.getVersion() + "[" + Versions.getFork() + "]", false);
				
		//Register own commands
		BukkitCommandManager manager = new BukkitCommandManager(this);
		
		manager.getCommandCompletions().registerCompletion("customitems", c -> {
			return this.customItemFactory.getRegisteredItems().stream().map(cui -> cui.getKey().getKey()).collect(Collectors.toList());
		});
		
		manager.getCommandContexts().registerContext(CustomItem.class, c -> {
			
			CustomItem item = this.customItemFactory.getCustomItem(c.popFirstArg());
			if (item == null) {
				throw new InvalidCommandArgument("Could not find an CustomItem with that name.");
			}
			return item;
		});
		
		manager.registerCommand(new APICommand(this));
		
		Metrics metrics = new Metrics(this);
		
		
		metrics.addCustomChart(new Metrics.SimplePie("economy", () -> {
			if (this.econEnabled) {
				return this.econ.getName();
			} else {
				return "Not installed";
			}
		}));
		
		
		SpigetUpdate update = new SpigetUpdate(API.getInstance(), 24842);
		update.checkForUpdate(new UpdateCallback() {
			@Override
			public void updateAvailable(String newVersion, String downloadUrl, boolean canAutoDownload) {
				Bukkit.getConsoleSender().sendMessage("§8-------------------------------------");
				Bukkit.getConsoleSender().sendMessage("");
				Bukkit.getConsoleSender().sendMessage("§8CT-Core version §9" + newVersion + " §8is available.");
				Bukkit.getConsoleSender().sendMessage("§8Download at: §7" + downloadUrl);
				Bukkit.getConsoleSender().sendMessage("");
				Bukkit.getConsoleSender().sendMessage("§8-------------------------------------");
			}
			@Override
			public void upToDate() { }
		});
		
	}

	@Override
	public void onDisable() {
		EnumLang.clean();
		if (getConfig().getBoolean("useRedis", false)) {
			this.getRedisManager().close();
		}
		this.persistentHandler.shutdown();
	}

	/**
	 * Base logging
	 * @param message the logged message.
	 */
	public void log(String message) {
		this.getLogger().info(message);
	}
	
	/**
	 * Spigot Console logging.
	 * @param plugin the plugin to log for.
	 * @param message the logged message.
	 */
	public void logToConsole(JavaPlugin plugin, String message) {
		this.logToConsole(plugin, message, true);
	}
	
	/**
	 * Spigot Console logging.
	 * @param plugin the plugin to log for.
	 * @param message the logged message.
	 * @param prefix the logging prefix.
	 */
	public void logToConsole(JavaPlugin plugin, String message, boolean prefix) {
		if (prefix) {
			Bukkit.getConsoleSender().sendMessage( "[" + plugin.getName() + "] " + message);
		} else {
			Bukkit.getConsoleSender().sendMessage(message);
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see net.crytec.IMulti#runAsync(java.lang.Runnable)
	 */
	@Override
	public void runAsync(Runnable runnable) {
		Bukkit.getScheduler().runTaskAsynchronously(this, runnable);
	}
	
	private void broadcastHooks() {
		this.hooks.forEach( (s, b) -> this.logToConsole(this, s.getDisplay() + ": "  + (b ? "§2Compatible" : "§4Not found." ), false));
	}
	
	@Override
	public void reloadConfig() {
		super.reloadConfig();
		String MAIN = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.main", "&9%module%&7> "));
		String ERROR = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.error", "&4Fehler&7> "));
		String NAME = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.name", "&6%var%&7"));
		String ELEM = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.element", "&d%var%&7"));
		String ON = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.on", "&2On"));
		String OFF = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.off", "&cOff"));
		String YES = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.yes", "&2Yes"));
		String NO = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.no", "&2No"));

		F.init(MAIN, ERROR, NAME, ELEM, ON, OFF, YES, NO);
	}
		
	private enum Hooks {
		REDIS("§8Redis"), NBT("§8Nbt"), SCOREBOARDAPI("§8ScoreboardAPI"), WORLDGUARD("§8Worldguard"), PROTOCOLLIB("§8ProtocolLib");

		@Getter
		private String display;

		private Hooks(String displayname) {
			this.display = displayname;
		}
	}
	
	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = (Economy) rsp.getProvider();
		return econ != null;
	}
}