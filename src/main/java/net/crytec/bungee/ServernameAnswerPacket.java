package net.crytec.bungee;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonObject;

import net.crytec.api.redis.RedisManager;
import net.crytec.api.redis.RedisManager.ServerType;
import net.crytec.api.redis.RedisPacketProvider;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class ServernameAnswerPacket implements RedisPacketProvider {
	
	private static RedisManager manager;
	
	public ServernameAnswerPacket(RedisManager manager) {
		ServernameAnswerPacket.manager = manager;
		this.server = "";
		this.port = 0;
	}
	
	public ServernameAnswerPacket(String server, int port) {
		this.server = server;
		this.port = port;
	}
	
	private final String server;
	private final int port;

	@Override
	public String getID() {
		return "servername";
	}

	@Override
	public ServerType getReceiverType() {
		return ServerType.SPIGOT;
	}

	@Override
	public JsonObject getPacketData() {
		JsonObject obj = new JsonObject();
		obj.addProperty("servername", this.server);
		obj.addProperty("port", this.port);
		return obj;
	}

	@Override
	public void handlePacketReceive(JsonObject data) {
		int port = data.get("port").getAsInt();
		Optional<ServerInfo> server = ProxyServer.getInstance().getServers().values().stream().filter(info -> (info.getAddress().getPort() == port)).findFirst();
		
		if (server.isPresent()) {
			ServerInfo si = server.get();
			ProxyServer.getInstance().getLogger().info("Server " + si.getName() + " is requesting ID..sending");
			ProxyServer.getInstance().getScheduler().schedule(BungeeAPI.getInstance(), () -> {
				manager.sendOutgoingMessage(new ServernameAnswerPacket(si.getName(), si.getAddress().getPort()));
			}, 2, TimeUnit.SECONDS);
		}
	}
}
