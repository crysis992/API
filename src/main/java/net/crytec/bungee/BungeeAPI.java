package net.crytec.bungee;

import net.crytec.IMulti;
import net.crytec.api.redis.RedisManager;
import net.crytec.api.redis.RedisManager.ServerType;
import net.crytec.api.util.BungeeConfig;
import net.crytec.api.util.F;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;

public class BungeeAPI extends Plugin implements IMulti {

	private RedisManager redis;
	private BungeeConfig config;
	
	private static BungeeAPI api;
		
	@Override
	public void onEnable() {
		api = this;
		
		this.log("§a[API] Loading API in Bungee Mode..");
		
		this.config = new BungeeConfig(this);
		this.config.load();
		
		this.setConfigEntry("format.main", "&9%module%&7> ");
		this.setConfigEntry("format.error", "&4Fehler&7> ");
		this.setConfigEntry("format.name", "&6%var%&7");
		this.setConfigEntry("format.element", "&6%var%&7");
		
		String MAIN = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.main", "&9%module%&7> "));
		String ERROR = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.error", "&4Fehler&7> "));
		String NAME = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.name", "&6%var%&7"));
		String ELEM = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.element", "&d%var%&7"));
		String ON = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.on", "&2On"));
		String OFF = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.off", "&cOff"));
		String YES = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.yes", "&2Yes"));
		String NO = ChatColor.translateAlternateColorCodes('&', getConfig().getString("format.no", "&2No"));

		F.init(MAIN, ERROR, NAME, ELEM, ON, OFF, YES, NO);
		
		this.getDataFolder().mkdirs();		
		this.redis = new RedisManager(this);
		
		this.redis.registerProvider(new ServernameAnswerPacket(this.redis));
		this.redis.setServer("BungeeCord");
		
	}
	
	
	@Override
	public void onDisable() {
		if (this.redis != null) {
			redis.close();
		}
	}
	
    public RedisManager getRedis() {
        return redis;
    }

	public void log(String message) {
		ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacyText(message));
	}

	@Override
	public void runAsync(Runnable runnable) {
		this.getProxy().getScheduler().runAsync(this, runnable);
	}
	
	public Configuration getConfig() {
		return this.config.getConfig();
	}
	
	public void saveConfig() {
		this.config.save();
	}
	
	public static BungeeAPI getInstance() {
		return BungeeAPI.api;
	}
	
	@Override
	public ServerType getServerType() {
		return ServerType.BUNGEE;
	}
	
	public void setConfigEntry(String path, Object object) {
		if (!this.config.isSet(path)) {
			System.out.println("Updated Config. [" + path + "] - Object: " + object);
			this.getConfig().set(path, object);
			this.saveConfig();
		}
	}
}