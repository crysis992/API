package net.crytec;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.Subcommand;
import net.crytec.api.itemstack.customitems.CustomItem;
import net.crytec.api.util.UtilPlayer;

@CommandAlias("ctcore|api")
@CommandPermission("ctcore.admin")
public class APICommand extends BaseCommand {

	private final API plugin;
	
	public APICommand(API plugin) {
		this.plugin = plugin;
	}
	
	@Default
	public void addCommand(CommandIssuer issuer) {
		issuer.sendMessage(ChatColor.GOLD + "=========== API ===========");
		issuer.sendMessage("§7API Version " + plugin.getDescription().getVersion());
		issuer.sendMessage("/api debug - Enable the debug mode (This may spam you)");
		issuer.sendMessage("/api reload - Reload the configuration");
		issuer.sendMessage(ChatColor.GOLD + "===========================");
	}
		
	@Subcommand("cui")
	@Description("Give yourself a custom item")
	@CommandCompletion("@customitems")
	public void giveCustomItem(Player sender, CustomItem item, @Default("1") Integer amount) {
		ItemStack i = item.getItemStack();
		i.setAmount(amount);
		sender.getInventory().addItem(i);
		UtilPlayer.playSound(sender, Sound.ENTITY_ITEM_PICKUP);
		sender.sendMessage("§7Gave yourself " + amount + "x §6" + item.getKey().getKey());
		return;
	}
	
	@Subcommand("reload")
	@Description("Reload CT-Core's config.yml")
	public void reloadConfiguration(CommandIssuer issuer) {
		plugin.reloadConfig();
		issuer.sendMessage(ChatColor.DARK_GREEN + "Reloaded configuration file.");
	}
}