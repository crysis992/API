package net.crytec.api.scoreboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.comphenix.protocol.wrappers.EnumWrappers.ScoreboardAction;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardDisplayObjective;
import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardObjective;
import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardObjective.HealthDisplay;
import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardScore;
import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardTeam;

public class PlayerBoard implements Scoreboard {

	private static ArrayList<String> teams = (ArrayList<String>) IntStream.rangeClosed(0, 15).mapToObj(i -> ChatColor.values()[i].toString()).collect(Collectors.toList());
	private final Player player;
	
	private HashMap<Integer, String> cache = Maps.newHashMap();
	
	private boolean activated;
	private ScoreboardHandler handler;
	private boolean hidden = false;
	
	private String title;
	
	public PlayerBoard(Player player) {
		this.player = player;
	}
	
	@Override
	public void activate() {
		if (activated)
			return;
		if (handler == null)
			throw new IllegalStateException("Scoreboard handler not set");

		this.activated = true;
		
		this.cache.clear();
		
		// Register Teams
		for (int i = 0; i < 15; i++) {
			WrapperPlayServerScoreboardTeam line = new WrapperPlayServerScoreboardTeam();
			line.setName(teams.get(i));
			line.setPlayers(Arrays.asList(teams.get(i)));
			line.setMode(0);
			line.sendPacket(this.player);
		}
		
		if (hidden) {
			WrapperPlayServerScoreboardObjective display = new WrapperPlayServerScoreboardObjective();
			display.setName("sidebar");
			display.setDisplayName(WrappedChatComponent.fromText(this.title));
			display.setMode(0);
			display.sendPacket(player);
			
			WrapperPlayServerScoreboardDisplayObjective obj = new WrapperPlayServerScoreboardDisplayObjective();
			obj.setPosition(1);
			obj.setScoreName("sidebar");
			obj.sendPacket(this.player);
		}
	}

	@Override
	public void deactivate() {
		if (!activated)
			return;

		activated = false;
		if (!this.player.isOnline()) return;
		
		for (int i = 0; i < 15; i++) {
			WrapperPlayServerScoreboardTeam line = new WrapperPlayServerScoreboardTeam();
			line.setName(teams.get(i));
			line.setPlayers(Arrays.asList(teams.get(i)));
			line.setMode(1);
			line.sendPacket(this.player);
		}
		
		WrapperPlayServerScoreboardObjective display = new WrapperPlayServerScoreboardObjective();
		display.setName("sidebar");
		display.setMode(1);
		display.sendPacket(player);
		this.hidden = true;
		
	}
	
	public void updateLine(int line, String text) {
		if (!cache.containsKey(line)) {
			this.showLine(line);
		}
		WrapperPlayServerScoreboardTeam update = new WrapperPlayServerScoreboardTeam();
		update.setName(teams.get(line));
		update.setMode(2);
		update.setSuffix(WrappedChatComponent.fromText(text));
		update.sendPacket(player);
		this.cache.put(line, text);
	}
	
	
	public void showLine(int line) {
		WrapperPlayServerScoreboardScore score = new WrapperPlayServerScoreboardScore();
		score.setObjectiveName("sidebar");
		score.setValue(line);
		score.setScoreboardAction(ScoreboardAction.CHANGE);
		score.setScoreName(teams.get(line));
		score.sendPacket(player);
	}

	public void hideLine(int line) {
		WrapperPlayServerScoreboardScore score = new WrapperPlayServerScoreboardScore();
		score.setObjectiveName("sidebar");
		score.setValue(line);
		score.setScoreboardAction(ScoreboardAction.REMOVE);
		score.setScoreName(teams.get(line));
		score.sendPacket(player);
	}
	
	
	public void setTitle(String title) {
		WrapperPlayServerScoreboardObjective obj = new WrapperPlayServerScoreboardObjective();
		obj.setName("sidebar");
		obj.setMode(2);
		obj.setHealthDisplay(HealthDisplay.INTEGER);
		obj.setDisplayName(WrappedChatComponent.fromText(title));
		obj.sendPacket(this.player);
	}
	
	public void update() {
		
		if (!player.isOnline()) {
			this.deactivate();
			return;
		}

		// Update Title
		
        String handlerTitle = handler.getTitle(player);
        String finalTitle = ChatColor.translateAlternateColorCodes('&', handlerTitle != null ? handlerTitle : ChatColor.BOLD.toString());
		
        if (!this.title.equals(finalTitle)) {
        	this.setTitle(finalTitle);
        	this.title = finalTitle;
        }
        
		List<Integer> appeared = Lists.newArrayList();
		
		for (Entry entry : handler.getEntries(player)) {
			String key = entry.getName();
			Integer score = entry.getPosition();
			
			if (key == null) continue;
			appeared.add(score);
			if (cache.containsKey(score) && cache.get(score).equals(key)) continue;
			this.updateLine(score, key);
		}

		List<Integer> remove = cache.keySet().stream().filter(l -> !appeared.contains(l)).collect(Collectors.toList());
		remove.forEach(l -> {
			this.hideLine(l);
			cache.remove(l);
		});
	}

	@Override
	public boolean isActivated() {
		return this.activated;
	}

	@Override
	public ScoreboardHandler getHandler() {
		return this.handler;
	}

	@Override
	public Scoreboard setHandler(ScoreboardHandler handler) {
		this.handler = handler;
		this.title = handler.getTitle(player);
		return this;
	}
	
	@Override
	public Player getHolder() {
		return this.player;
	}
}
