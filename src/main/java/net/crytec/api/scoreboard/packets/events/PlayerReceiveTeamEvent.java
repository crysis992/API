package net.crytec.api.scoreboard.packets.events;

import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.scoreboard.Team;

import com.comphenix.protocol.wrappers.WrappedChatComponent;

import lombok.Getter;
import lombok.Setter;
import net.crytec.api.scoreboard.BoardManager;
import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardTeam;

public class PlayerReceiveTeamEvent extends Event {
	
	public PlayerReceiveTeamEvent(Player receiver, WrappedChatComponent prefix, WrappedChatComponent suffix, ChatColor color, WrapperPlayServerScoreboardTeam wrapper) {
		this.prefix = prefix;
		this.suffix = suffix;
		this.color = color;
		this.receiver = receiver;
		this.wrapper = wrapper;
	}
	
	@Getter
	private final Player receiver;
	
	@Getter @Setter
	private WrappedChatComponent prefix;
	
	@Getter @Setter
	private WrappedChatComponent suffix;
	
	@Getter @Setter
	private ChatColor color;
	
	@Getter
	private WrapperPlayServerScoreboardTeam wrapper;

	private static final HandlerList handlers = new HandlerList();


	public Optional<Player> getTarget() {
		if (wrapper.getPlayers().isEmpty()) {
			Optional<Team> t = BoardManager.get().getUsers().values().stream().filter(entry -> entry.getName().equals(this.wrapper.getName())).findFirst();
			if (t.isPresent()) {
				Team team = t.get();

				if (team.getEntries().isEmpty()) return Optional.empty();
				
				Player target = Bukkit.getPlayerExact(team.getEntries().iterator().next());
				return (target == null) ? Optional.empty() : Optional.of(target);
			} else {
				return Optional.empty();
			}
		}

		Player target = Bukkit.getPlayerExact(wrapper.getPlayers().get(0));
		return (target == null) ? Optional.empty() : Optional.of(target);
	}
	
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
