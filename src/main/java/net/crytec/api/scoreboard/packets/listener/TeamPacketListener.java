package net.crytec.api.scoreboard.packets.listener;

import org.bukkit.Bukkit;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import net.crytec.api.scoreboard.BoardManager;
import net.crytec.api.scoreboard.packets.WrapperPlayServerScoreboardTeam;
import net.crytec.api.scoreboard.packets.events.PlayerReceiveTeamEvent;

public class TeamPacketListener extends PacketAdapter{

	public TeamPacketListener(BoardManager manager) {
		super(manager.getPlugin(), ListenerPriority.HIGHEST, PacketType.Play.Server.SCOREBOARD_TEAM);
	}
	
	@Override
	public void onPacketSending(PacketEvent event) {
		if(!event.getPacketType().equals(PacketType.Play.Server.SCOREBOARD_TEAM)) return;
		PacketContainer packet = event.getPacket();
		WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam(packet);
		
		PlayerReceiveTeamEvent teamEvent = new PlayerReceiveTeamEvent(event.getPlayer(), wrapper.getPrefix(), wrapper.getSuffix(), wrapper.getColor(), wrapper);
		Bukkit.getPluginManager().callEvent(teamEvent);

		wrapper.setColor(teamEvent.getColor());
		wrapper.setPrefix(WrappedChatComponent.fromJson(teamEvent.getPrefix().getJson()));
		wrapper.setSuffix(teamEvent.getSuffix());
	}
	
}
