package net.crytec.api.scoreboard;

public class Entry {

	private String name;
	private int position;

	public Entry(String name, int position) {
		this.position = position;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
