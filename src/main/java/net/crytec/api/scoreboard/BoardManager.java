package net.crytec.api.scoreboard;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.Setter;
import net.crytec.API;
import net.crytec.api.scoreboard.packets.listener.TeamPacketListener;

public class BoardManager implements Listener {

	private static BoardManager instance;
	
	@Getter
	private HashMap<UUID, Team> users = Maps.newHashMap();
	private HashMap<UUID, PlayerBoard> boards = Maps.newHashMap();
	
	@Getter//Temp
	private final Scoreboard titleboard;
	@Getter
	private final API plugin;
	private final ProtocolManager protoManager;
	
	@Getter @Setter
	public boolean customHandler = false;
	@Getter @Setter
	public JavaPlugin handler;
	
	public static BoardManager get() {
		if (instance != null) return instance;
		else {
			instance = new BoardManager();
			return instance;
		}
	}
	

	private BoardManager() {
		this.plugin = API.getInstance();
		
		if (!plugin.getConfig().getBoolean("modules.scoreboard", false)) {
			throw new UnsupportedOperationException("Scoreboard API is disabled in the configuration.");
		}
		
		this.protoManager = ProtocolLibrary.getProtocolManager();
		protoManager.addPacketListener(new TeamPacketListener(this));
//		perms = LuckPerms.getApi();
		
		this.titleboard = Bukkit.getScoreboardManager().getNewScoreboard();
		Bukkit.getPluginManager().registerEvents(this, API.getInstance());
		
		Objective obj = this.titleboard.registerNewObjective("sidebar", "dummy", " ");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(API.getInstance(), new Runnable() {
			@Override
			public void run() {
				for (PlayerBoard board : boards.values()) {
					if (board.isActivated()) {
						board.update();
					}
				}
			}
		}, 20, 10);
	}
	
	public void addPlayer(Player player, String priority) {
		if (this.users.containsKey(player.getUniqueId())) {
			this.unregisterPlayer(player);
		}
		
		String name = player.getName();
		
		if (name.length() > 14) {
			name = name.substring(0, 14);
		}
		
		if (priority.length() > 2) {
			priority = priority.substring(0, 2);
		}
		
		Team team = this.titleboard.registerNewTeam(priority + name);
		
		team.setPrefix("");
		team.setSuffix("");
		team.setColor(ChatColor.WHITE);
		team.addEntry(player.getName());
		this.users.put(player.getUniqueId(), team);
		
		player.setScoreboard(this.titleboard);
		this.boards.put(player.getUniqueId(), new PlayerBoard(player));
	}
	
	public void addPlayer(Player player) {
		this.addPlayer(player, "0");
	}

	public void unregisterPlayer(Player player) {
		if (!this.users.containsKey(player.getUniqueId())) return;
		this.titleboard.getTeam(this.users.get(player.getUniqueId()).getName()).unregister();
		player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
		users.remove(player.getUniqueId());
		boards.get(player.getUniqueId()).deactivate();
		boards.remove(player.getUniqueId());
	}
	
	public void setBoard(Player player, ScoreboardHandler handler) {
		PlayerBoard board = this.getBoard(player);
		if (board.isActivated()) {
			board.deactivate();
		}
		board.setHandler(handler);
		board.activate();
	}
	
	public PlayerBoard getBoard(Player player) {
		return this.boards.get(player.getUniqueId());
	}
	
	public void resetBoard(Player player) {
		this.getBoard(player).deactivate();
	}
	
	public void setPrefix(Player player, String prefix) {
		this.users.get(player.getUniqueId()).setPrefix(prefix);
	}
	
	public void setSuffix(Player player, String suffix) {
		this.users.get(player.getUniqueId()).setSuffix(suffix);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if (this.customHandler) return;
		this.addPlayer(event.getPlayer());
	}
	
	@EventHandler
	public void onJoin(PlayerQuitEvent event) {
		if (this.customHandler) return;
		this.unregisterPlayer(event.getPlayer());
	}
}