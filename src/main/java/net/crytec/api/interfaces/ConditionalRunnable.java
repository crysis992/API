package net.crytec.api.interfaces;

public interface ConditionalRunnable extends Runnable {
	
	
	public void run();
	
	public boolean canCancel();

	public default void onCancel() {
		return;
	}
	
	public default void onRunout() {
		return;
	}
}
