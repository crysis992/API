package net.crytec.api.config.playerdata.data;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

public abstract class PlayerData {
    
    @Getter
    private JavaPlugin plugin;
    @Getter
    private UUID owner;
    @Getter
    private YamlConfiguration config;
    @Getter
    private File file;
    
    public void writeDefaults(YamlConfiguration config) {
	
    }
    
    public final void initialize(JavaPlugin plugin, UUID owner, File file, YamlConfiguration config) {
	this.plugin = plugin;
	this.owner = owner;
	this.config = config;
	this.file = file;
	
    }
    
    public abstract void initialize();
    
    public boolean isOwnerOnline() {
	return Bukkit.getPlayer(this.owner) != null;
    }
    
    public boolean saveConfig() {
	try {
	    this.config.save(this.file);
	    return true;
	} catch (IOException e) {
	    e.printStackTrace();
	    return false;
	}
    }
    
    public abstract void saveData(YamlConfiguration config);
    
    public abstract void loadData(YamlConfiguration config);
}
