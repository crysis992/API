package net.crytec.api.itemstack;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class LeatherArmorBuilder extends ItemBuilder {

	private LeatherArmorMeta meta;;

	public LeatherArmorBuilder(ItemStack is) {
		super(is);
		this.meta = (LeatherArmorMeta) is.getItemMeta();
	}

	public LeatherArmorBuilder(Material material) {
		super(material);
		this.meta = (LeatherArmorMeta) is.getItemMeta();
	}

	public LeatherArmorBuilder setColor(Color color) {
		this.meta.setColor(color);
		return this;
	}
	
	public LeatherArmorBuilder setColor(int red, int green, int blue) {
		this.meta.setColor(Color.fromRGB(red, green, blue));
		return this;
	}
	

	@Override
	public ItemStack build() {
		is.setItemMeta(this.meta);
		return super.build();

	}
}