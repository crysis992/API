package net.crytec.api.itemstack;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.meta.tags.ItemTagType;

import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import net.crytec.api.nbt.NBTItem;
 
public class ItemBuilder {
		
	protected ItemStack	                           is;
	
	/**
	 * Inits the builder with the given {@link Material}
	 * 
	 * @param mat
	 *            the {@link Material} to start the builder from
	 */
	public ItemBuilder(final Material mat) {
		this.is = (mat == Material.AIR) ? new ItemStack(Material.BARRIER) : new ItemStack(mat);
	}
	
	/**
	 * Inits the builder with the given {@link ItemStack}
	 * 
	 * @param is the {@link ItemStack} to start the builder from
	 */
	public ItemBuilder(final ItemStack is) {
		this.is = is;
	}
	
	/**
	 * Changes the amount of the {@link ItemStack}
	 * 
	 * @param amount the new amount to set
	 * @return this builder for chaining
	 */
	public ItemBuilder amount(final int amount) {
		is.setAmount(amount);
		return this;
	}
	
	/**
	 * Changes the display name of the {@link ItemStack}
	 * 
	 * @param name the new display name to set
	 * @return this builder for chaining
	 */
	public ItemBuilder name(final String name) {
		final ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Changes the display name of the {@link ItemStack}
	 * 
	 * @param name the new display name to set
	 * @param colorCode 
	 * @return this builder for chaining
	 */
	public ItemBuilder name(final String name, char colorCode) {
		final ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes(colorCode, name));
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Adds a new line to the lore of the {@link ItemStack}
	 * 
	 * @param name the new line to add
	 * @return this builder for chaining
	 */
	public ItemBuilder lore(final String name) {
		final ItemMeta meta = is.getItemMeta();
		List<String> lore = meta.hasLore() ? meta.getLore() : Lists.newArrayList();
		lore.add(name);
		meta.setLore(lore);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 *  Adds a new line to the lore of the {@link ItemStack} while replacing the current lore
	 * @param name
	 * @return this builder for chaining
	 */
	public ItemBuilder loreReplace(final String name) {
		final ItemMeta meta = is.getItemMeta();
		List<String> lore = new ArrayList<String>();
		lore.add(name);
		meta.setLore(lore);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Adds a ArrayList to the lore of the {@link ItemStack}
	 * 
	 * @param lore the new line to add
	 * @return this builder for chaining
	 */
	public ItemBuilder lore(final List<String> lore) {
		final ItemMeta meta = is.getItemMeta();
		List<String> curLore = meta.hasLore() ? meta.getLore() : Lists.newArrayList();
		curLore.addAll(lore);
		meta.setLore(curLore);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Adds a ArrayList to the lore of the {@link ItemStack}
	 * 
	 * @param lore the new line to add
	 * @return this builder for chaining
	 */
	public ItemBuilder loreReplace(final List<String> lore) {
		final ItemMeta meta = is.getItemMeta();
		meta.setLore(lore);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Make {@link ItemStack} unbreakable
	 * 
	 * @param unbreakable unbreakable status.
	 * @return this builder for chaining
	 */
	public ItemBuilder setUnbreakable(final boolean unbreakable) {
		final ItemMeta meta = is.getItemMeta();
		meta.setUnbreakable(unbreakable);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Changes the durability of the {@link ItemStack}
	 * 
	 * @param durability the new durability to set
	 * @return this builder for chaining
	 */
	public ItemBuilder setDurability(final int durability) {
		final ItemMeta meta = is.getItemMeta();
		((Damageable) meta).setDamage(durability);
		is.setItemMeta(meta);
		return this;
	}
		
	/**
	 * Adds an {@link Enchantment} with the given level to the {@link ItemStack}
	 * 
	 * @param enchantment
	 *            the enchantment to add
	 * @param level the level of the enchantment
	 * @return this builder for chaining
	 */
	public ItemBuilder enchantment(final Enchantment enchantment, final int level) {
		if (level <= 0) {
			is.removeEnchantment(enchantment);
		} else {
			is.addUnsafeEnchantment(enchantment, level);
		}
		return this;
	}
	
	/**
	 * Adds an {@link Enchantment} with the level 1 to the {@link ItemStack}
	 * 
	 * @param enchantment the enchantment to add
	 * @return this builder for chaining
	 */
	public ItemBuilder enchantment(final Enchantment enchantment) {
		is.addUnsafeEnchantment(enchantment, 1);
		return this;
	}
	
	/**
	 * Changes the {@link Material} of the {@link ItemStack}
	 * 
	 * @param material the new material to set
	 * @return this builder for chaining
	 */
	public ItemBuilder type(final Material material) {
		is.setType(material);
		return this;
	}
	
	/**
	 * Clears the lore of the {@link ItemStack}
	 * 
	 * @return this builder for chaining
	 */
	public ItemBuilder clearLore() {
		final ItemMeta meta = is.getItemMeta();
		meta.setLore(new ArrayList<String>());
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Clears the list of {@link Enchantment}s of the {@link ItemStack}
	 * 
	 * @return this builder for chaining
	 */
	public ItemBuilder clearEnchantments() {
		for (final Enchantment e : is.getEnchantments().keySet()) {
			is.removeEnchantment(e);
		}
		return this;
	}
	
	/**
	 * Sets the {@link OfflinePlayer} of the skull item
	 * This may call a blocking web request!
	 * 
	 * @param offlinePlayer {@link OfflinePlayer} of the texture.
	 * @return this builder for chaining
	 */
	public ItemBuilder setSkullOwner(OfflinePlayer offlinePlayer) {
		if (is.getType() == Material.PLAYER_HEAD) {
			SkullMeta meta = (SkullMeta) is.getItemMeta();
			meta.setOwningPlayer(offlinePlayer);
			is.setItemMeta(meta);
			return this;
		} else {
			throw new IllegalArgumentException("skullOwner() only applicable for skulls!");
		}
	}
	
	/**
	 * Set the Skullowner/head texture via URL from the minecraft texture
	 * servers.
	 * 
	 * @param url
	 *            - URL to the skin/head texture - Only official minecraft
	 *            textureserver links are allowed.
	 * @return this builder for chaining
	 */
	public ItemBuilder setSkullURL(String url) {
		if (is.getType() == Material.PLAYER_HEAD) {
			if (url.isEmpty()) return this;

			SkullMeta headMeta = (SkullMeta) is.getItemMeta();
			GameProfile profile = new GameProfile(UUID.randomUUID(), null);
			byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
			profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
			Field profileField = null;
			try {
				profileField = headMeta.getClass().getDeclaredField("profile");
				profileField.setAccessible(true);
				profileField.set(headMeta, profile);
			} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
				e1.printStackTrace();
			}
			is.setItemMeta(headMeta);
			return this;
		} else {
			throw new IllegalArgumentException("skullOwner() only applicable for skulls!");
		}
	}
		
	/**
	 * Sets the given ItemFlag
	 * @param flag
	 * @return this builder for chaining
	 */
	public ItemBuilder setItemFlag(ItemFlag flag) {
		final ItemMeta meta = is.getItemMeta();
		meta.addItemFlags(flag);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Add a new String NBT Tag with the given key and value
	 * @param key
	 * @param value
	 * @return this builder for chaining
	 */
	public ItemBuilder addNBTString(String key, String value) {
		NBTItem nbt = new NBTItem(is);
		nbt.setString(key, value);
		is = nbt.getItem();
		return this;
	}
	
	/**
	 * Add a new double NBT Tag with the given key and value
	 * @param key
	 * @param value
	 * @return this builder for chaining
	 */
	public ItemBuilder addNBTDouble(String key, double value) {
		NBTItem nbt = new NBTItem(is);
		nbt.setDouble(key, value);
		is = nbt.getItem();
		return this;
	}
	
	/**
	 * Add a new boolean NBT Tag with the given key and value
	 * @param key
	 * @param value
	 * @return this builder for chaining
	 */
	public ItemBuilder addNBTBoolean(String key, boolean value) {
		NBTItem nbt = new NBTItem(is);
		nbt.setBoolean(key, value);
		is = nbt.getItem();
		return this;
	}
	
	/**
	 * Add a new Integer NBT Tag with the given key and value
	 * @param key
	 * @param value
	 * @return this builder for chaining
	 */
	public ItemBuilder addNBTInt(String key, int value) {
		NBTItem nbt = new NBTItem(is);
		nbt.setInteger(key, value);
		is = nbt.getItem();
		return this;
	}
	
	/**
	 * Add a new Long NBT Tag with the given key and value
	 * @param key
	 * @param value
	 * @return this builder for chaining
	 */
	public ItemBuilder addNBTLong(String key, long value) {
		NBTItem nbt = new NBTItem(is);
		nbt.setLong(key, value);
		is = nbt.getItem();
		return this;
	}
	
	
	/**
	 * Add a attribute to this itemstack
	 * 
	 * @param attribute
	 * @param amount
	 * @param slot
	 * @return this builder for chaining
	 */
	public ItemBuilder setAttribute(Attribute attribute, double amount, EquipmentSlot slot) {
		ItemMeta meta = is.getItemMeta();
		AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "itembuilder", amount, Operation.ADD_NUMBER, slot);
		meta.addAttributeModifier(attribute, modifier);
		is.setItemMeta(meta);
		return this;
	}
	
	/**
	 * Remove a attribute from an ItemStack
	 * 
	 * @param attribute
	 * @return this builder for chaining
	 */
	public ItemBuilder removeAttribute(Attribute attribute) {
		ItemMeta meta = is.getItemMeta();
		meta.removeAttributeModifier(attribute);
		is.setItemMeta(meta);
		return this;
	}
	
	public <T, Z> ItemBuilder setTag(NamespacedKey key, ItemTagType<T, Z> type, Z value) {
	    ItemMeta meta = is.getItemMeta();
	    meta.getCustomTagContainer().setCustomTag(key, type, value);
	    is.setItemMeta(meta);
	    return this;
	}
	
	/**
	 * Builds the {@link ItemStack}
	 * 
	 * @return the created {@link ItemStack}
	 */
	public ItemStack build() {
		return is;
	}
}
