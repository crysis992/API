package net.crytec.api.itemstack.customitems;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import net.crytec.API;
import net.crytec.api.nbt.NBTItem;
import net.crytec.api.recharge.Recharge;

public class CustomItemManager implements Listener {
	public static final String NBT_COMPOUND_KEY = "custom_item";
	public static final String NBT_TYPE_KEY = "type";
	
	private final HashMap<String, CustomItem> registeredItems = Maps.newHashMap();
	
	
	public CustomItemManager(API instance) {
		Bukkit.getPluginManager().registerEvents(this, instance);
	}
	
	
	public void registerItem(CustomItem item) {
		NBTItem  nbt = new NBTItem(item.getItemStack());
		Validate.notNull(nbt.getCompound(NBT_COMPOUND_KEY), "NBTCompound must be set to register this item");
		Validate.notNull(nbt.getCompound(NBT_COMPOUND_KEY).getString(NBT_TYPE_KEY), "Type must be set inside the NBT Compound");
		Validate.isTrue(nbt.getCompound(NBT_COMPOUND_KEY).getString(NBT_TYPE_KEY).equals(item.getKey().getKey()), "The NBT does not match.");
		this.registeredItems.put(item.getKey().getKey(), item);
	}
	
	public CustomItem getCustomItem(String type) {
		return this.registeredItems.get(type);
	}
	
	public CustomItem getCustomItemByKey(NamespacedKey key) {
		return this.registeredItems.get(key.getKey());
	}
	
	public Collection<CustomItem> getRegisteredItems() {
		return this.registeredItems.values();
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.isCancelled()) return;
		ItemStack item = event.getItemInHand();
		if (item == null) return;
		NBTItem nbt = new NBTItem(item);
		if (!nbt.hasKey(NBT_COMPOUND_KEY)) return;
		CustomItem customItem = this.registeredItems.get(nbt.getCompound(NBT_COMPOUND_KEY).getString(NBT_TYPE_KEY));
		
		if (!(customItem instanceof CustomPlaceableItem)) return;
		((CustomPlaceableItem) customItem).onPlace(event);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onInteract(PlayerInteractEvent event) {
		if (event.getHand() != EquipmentSlot.HAND || event.getAction() == Action.PHYSICAL) return;
		if (event.getItem() == null || event.getPlayer().getInventory().getItemInMainHand() == null) return;
		NBTItem nbt = new NBTItem(event.getItem());
		if (!nbt.hasKey(NBT_COMPOUND_KEY)) return;
		
		CustomItem item = this.registeredItems.get(nbt.getCompound(NBT_COMPOUND_KEY).getString(NBT_TYPE_KEY));
		if (item == null) return;
		if (item instanceof CustomPlaceableItem) return;
		
		event.setCancelled(true);
		
		if (item.getCooldown() > 0 && !Recharge.Instance.usable(event.getPlayer(), "cui-" + item.getKey())) {
			return;
		}
		
		item.onUse(event);
		
		if (item.getCooldown() > 0) {
			Recharge.Instance.use(event.getPlayer(), "cui-" + item.getKey(), item.getCooldown(), true, true);
		}
	}

}
