package net.crytec.api.itemstack.customitems;

import org.bukkit.NamespacedKey;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public abstract class CustomPlaceableItem extends CustomItem {

	public CustomPlaceableItem(NamespacedKey key) {
		super(key);
	}

	public abstract void onPlace(BlockPlaceEvent event);
	
	@Override
	public final void onUse(PlayerInteractEvent event) {
		// This is not a usable item
	}
	
}