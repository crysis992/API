package net.crytec.api.itemstack.customitems;

import org.bukkit.NamespacedKey;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import lombok.Getter;
import lombok.Setter;
import net.crytec.api.nbt.NBTCompound;
import net.crytec.api.nbt.NBTItem;

public abstract class CustomItem {
	
	public CustomItem(NamespacedKey key) {
		this.key = key;
		this.item = this.initItem();
		this.addNBTTags();
	}

	@Getter
	protected final NamespacedKey key;
	
	@Setter
	protected ItemStack item;
	@Getter
	public long cooldown = 0;
	
	protected abstract ItemStack initItem();
	
	public final ItemStack getItemStack() {
		return this.item.clone();
	}
	
	protected final void addNBTTags() {
		NBTItem nbt = new NBTItem(this.item);
		NBTCompound compound = nbt.addCompound(CustomItemManager.NBT_COMPOUND_KEY);
		compound.setString(CustomItemManager.NBT_TYPE_KEY, this.key.getKey());
		this.item = nbt.getItem();
	}
	
	public abstract void onUse(PlayerInteractEvent event);
}