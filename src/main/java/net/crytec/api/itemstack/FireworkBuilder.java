package net.crytec.api.itemstack;

import org.apache.commons.lang.Validate;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkBuilder extends ItemBuilder {

	private FireworkMeta meta;;

	public FireworkBuilder(ItemStack is) {
		super(is);
		Validate.isTrue(is.getType() == Material.FIREWORK_ROCKET, "FireworkBuilder can only edit Material.FIREWORK_ROCKET");
		this.meta = (FireworkMeta) is.getItemMeta();
	}

	public FireworkBuilder(Material material) {
		super(material);
		Validate.isTrue(material == Material.FIREWORK_ROCKET, "FireworkBuilder can only edit Material.FIREWORK_ROCKET");
		this.meta = (FireworkMeta) is.getItemMeta();
	}

	public FireworkBuilder addEffect(FireworkEffect effect) {
		this.meta.addEffect(effect);
		return this;
	}
	
	public FireworkBuilder setPower(int power) {
		this.meta.setPower(power);
		return this;
	}
	
	public FireworkBuilder clearEffects() {
		this.meta.clearEffects();
		return this;
	}

	@Override
	public ItemStack build() {
		is.setItemMeta(this.meta);
		return super.build();

	}
}