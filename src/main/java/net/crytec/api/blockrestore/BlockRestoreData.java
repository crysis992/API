package net.crytec.api.blockrestore;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.type.Snow;

public class BlockRestoreData {
	protected Block _block;
	protected Material _fromMat;
	protected Material _toMat;
	protected long _expireDelay;
	protected long _epoch;
	protected long _meltDelay = 0L;
	protected long _meltLast = 0L;

	public BlockRestoreData(Block block, Material toMat, long expireDelay, long meltDelay) {
		this._block = block;
		
		this._fromMat = block.getType();
		this._toMat = toMat;

		this._expireDelay = expireDelay;
		this._epoch = System.currentTimeMillis();

		this._meltDelay = meltDelay;
		this._meltLast = System.currentTimeMillis();

		set();
	}

	public boolean expire() {
		if (System.currentTimeMillis() - this._epoch < this._expireDelay) {
			return false;
		}

		if (melt()) {
			return false;
		}

		restore();
		return true;
	}

	public boolean melt() {
		if ((this._block.getType() != Material.SNOW) && (this._block.getType() != Material.SNOW_BLOCK)) {
			return false;
		}
		if ((this._block.getRelative(BlockFace.UP).getType() == Material.SNOW) || (this._block.getRelative(BlockFace.UP).getType() == Material.SNOW_BLOCK)) {
			this._meltLast = System.currentTimeMillis();
			return true;
		}

		if (System.currentTimeMillis() - this._meltLast < this._meltDelay) {
			return true;
		}
		
		BlockState state = this._block.getState();
		Snow snow = (Snow) state.getBlockData();

		if (this._block.getType() == Material.SNOW_BLOCK) {
			this._block.setType(Material.SNOW);
			snow.setLayers(7);
			state.setBlockData(snow);
			state.update(true);
		}

		if (snow.getLayers() <= 0) {
			return false;
		}
		snow.setLayers(snow.getLayers() -1);
		state.setBlockData(snow);
		state.update();
		this._meltLast = System.currentTimeMillis();
		return true;
	}

	public void update(Material toMat, long expireTime) {
		this._toMat = toMat;

		set();

		this._expireDelay = expireTime;
		this._epoch = System.currentTimeMillis();
	}

	public void update(Material toMat, long expireTime, long meltDelay) {
		this._toMat = toMat;

		set();

		this._expireDelay = expireTime;
		this._epoch = System.currentTimeMillis();

		if (this._meltDelay < meltDelay) {
			this._meltDelay = ((this._meltDelay + meltDelay) / 2L);
		}
	}

	public void set() {
		this._block.setType(this._toMat);
	}

	public void restore() {
		this._block.setType(this._fromMat);
	}

	public void setFromMat(Material m) {
		this._fromMat = m;
	}
}
