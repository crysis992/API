package net.crytec.api.blockrestore;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import net.crytec.API;

/**
 * 
 * Deprecated class, usage is no longer recommended
 * 
 * @author crysis992
 *
 */
public class BlockRestore implements Listener, Runnable {
	private HashMap<Block, BlockRestoreData> _blocks = new HashMap<Block, BlockRestoreData>();

	private BlockRestore(API api) {
		if (initialized) throw new UnsupportedOperationException("BlockRestore cannot be instantiated");
		Bukkit.getPluginManager().registerEvents(this, api);
		Bukkit.getScheduler().runTaskTimer(api, this, 100L, 1L);
		BlockRestore.get = this;
		initialized = true;
	}
	
	public static BlockRestore get;
	
	private static boolean initialized;
	
	public static void initialize() {
		new BlockRestore(API.getInstance());
	}
	
	
	public static BlockRestore get() {
		return BlockRestore.get;
	}

	@EventHandler(priority = EventPriority.LOW)
	public void BlockBreak(BlockBreakEvent event) {
		if (contains(event.getBlock())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void BlockPlace(BlockPlaceEvent event) {
		if (contains(event.getBlockPlaced())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void Piston(BlockPistonExtendEvent event) {
		if (event.isCancelled()) {
			return;
		}
		Block push = event.getBlock();
		for (int i = 0; i < 13; i++) {
			push = push.getRelative(event.getDirection());

			if (push.getType() == Material.AIR) {
				return;
			}
			if (contains(push)) {
				push.getWorld().playEffect(push.getLocation(), Effect.STEP_SOUND, push.getType());
				event.setCancelled(true);
				return;
			}
		}
	}

	public void restore(Block block) {
		if (!contains(block)) {
			return;
		}
		((BlockRestoreData) this._blocks.remove(block)).restore();
	}

	public void add(Block block, Material toMaterial, long expireTime) {
		if (!contains(block))
			getBlocks().put(block, new BlockRestoreData(block, toMaterial, expireTime, 0L));
		else {
			getData(block).update(toMaterial, expireTime);
		}
	}

	public boolean contains(Block block) {
		if (getBlocks().containsKey(block))
			return true;
		return false;
	}

	public BlockRestoreData getData(Block block) {
		if (this._blocks.containsKey(block))
			return (BlockRestoreData) this._blocks.get(block);
		return null;
	}

	public HashMap<Block, BlockRestoreData> getBlocks() {
		return this._blocks;
	}

	@Override
	public void run() {
		ArrayList<Block> toRemove = new ArrayList<Block>();

		for (BlockRestoreData cur : this._blocks.values()) {
			if (cur.expire()) {
				toRemove.add(cur._block);
			}
		}
		for (Block cur : toRemove) {
			this._blocks.remove(cur);
		}
	}
}
