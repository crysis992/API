package net.crytec.api;

import java.util.HashMap;

import org.apache.commons.lang.Validate;
import org.bukkit.plugin.java.JavaPlugin;
/**
 * Used to create sub-plugins
 * 
 * @author crysis992
 *
 */
public class MiniPluginManager {

	private JavaPlugin plugin;
	private HashMap<Class<? extends MiniPlugin>, MiniPlugin> _addons = new HashMap<Class<? extends MiniPlugin>, MiniPlugin>();
	
	public MiniPluginManager(JavaPlugin plugin) {
		this.plugin = plugin;
	}
	
	/**
	 * Gets a map of all MiniPlugin classes and instances.
	 */
	public HashMap<Class<? extends MiniPlugin>, MiniPlugin> getAll() {
		return this._addons;
	}

	/**
	 * Registers the addon
	 * 
	 * @param addon
	 */
	public void registerAddon(MiniPlugin addon) {
		if (isEnabled(addon.getName())) {
			addon.enable();
			_addons.put(addon.getClass(), addon);
			return;
		}
	}

	/**
	 * Enables the addon, regardless of the configuration setting. Calling this
	 * method will make the addon always loaded.
	 * 
	 * @param addon
	 */
	public void enableDirect(MiniPlugin addon) {
		addon.enable();
		_addons.put(addon.getClass(), addon);
	}
	
	public <T extends MiniPlugin> T getAddon(Class<T> clazz) {
		Validate.notNull(clazz, "Null class cannot have a MiniPlugin");
		if (!MiniPlugin.class.isAssignableFrom(clazz)) {
			throw new IllegalArgumentException(clazz + " does not extend " + MiniPlugin.class);
		}

		MiniPlugin plugin = this._addons.get(clazz);
		return clazz.cast(plugin);
	}

	public void unregisterAddon(MiniPlugin addon) {
		addon.disable();
		_addons.remove(addon.getClass());
	}

	public void reloadAddon(MiniPlugin addon) {
		addon.disable();
		addon.enable();
	}

	public int getEnabledAddons() {
		return _addons.size();
	}

	public boolean isEnabled(String addonName) {
		if (!plugin.getConfig().isSet("Addons." + addonName)) {
			plugin.getConfig().set("Addons." + addonName, false);
			plugin.saveConfig();
		}
		return plugin.getConfig().getBoolean("Addons." + addonName);
	}
}
