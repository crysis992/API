package net.crytec.api.util;

import java.util.Optional;

import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UtilEnt {

	public static Material getSpawnEgg(EntityType type) {
		if (!EnumUtils.isValidEnum(Material.class, type.toString() + "_SPAWN_EGG"))
			return Material.BARRIER;
		return Material.valueOf(type.toString() + "_SPAWN_EGG");
	}

	public static boolean isInRadius(Entity entity, Location loc, double radius) {
		double x = loc.getX() - entity.getLocation().getX();
		double y = loc.getY() - entity.getLocation().getY();
		double z = loc.getZ() - entity.getLocation().getZ();
		double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));

		return (distance <= radius);
	}

	/**
	 * Returns the {@link SpawnReason} for the given entity.
	 * 
	 * @param entity
	 * @return Returns the {@link SpawnReason} for this entity. If no reason was
	 *         found, {@link SpawnReason.CUSTOM} will be returned
	 */
	public static SpawnReason getSpawnReason(Entity entity) {
		Optional<String> reason = entity.getScoreboardTags().stream().filter(tag -> tag.startsWith("ct:corespawnreason;")).findFirst();
		return reason.isPresent() ? SpawnReason.valueOf(reason.get().replace("ctcore:spawnreason;", "")) : SpawnReason.CUSTOM;
	}
}