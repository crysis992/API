package net.crytec.api.util;

import java.util.Objects;

import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import lombok.experimental.UtilityClass;
import net.crytec.API;
import net.crytec.api.interfaces.ConditionalRunnable;

@UtilityClass
public class Tasks {

	private static API plugin = API.getInstance();
	
	public static BukkitTask schedule(final ConditionalRunnable runnable, final int delay, final int interval, final int repeatitions) {
		Objects.requireNonNull(runnable, "ConditionalRunnable cannot be null");
		return new RepeatTask(runnable, repeatitions).runTaskTimer(plugin, delay, interval);
	}
	
	public static BukkitTask scheduleAsync(final ConditionalRunnable runnable, final int delay, final int interval, final int repeatitions) {
		Objects.requireNonNull(runnable, "ConditionalRunnable cannot be null");
		return new RepeatTask(runnable, repeatitions).runTaskTimerAsynchronously(plugin, delay, interval);
	}

	private static class RepeatTask extends BukkitRunnable {
		
		private final ConditionalRunnable runnable;
		private int repeat;

		public RepeatTask(final ConditionalRunnable runnable, final int repeat) {
			this.runnable = runnable;
			this.repeat = repeat;
		}
		
		@Override
		public void run() {
			
			if (runnable.canCancel()) {
				this.cancel();
				runnable.onCancel();
				return;
			}
			
			runnable.run();
			
			if (repeat == -1) return;
			else repeat--;
			
			if (repeat <= 0) {
				cancel();
				runnable.onCancel();
				runnable.onRunout();
			}
		}
	}
}