package net.crytec.api.util;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;

import lombok.experimental.UtilityClass;
import net.crytec.API;

@UtilityClass
public class F {
	
	private static String MAIN = "";
	private static String ERROR = "";
	private static String NAME = "";
	private static String ELEM = "";
	private static String ON = "";
	private static String OFF = "";
	private static String YES = "";
	private static String NO = "";
	
	public static void init(String main, String error, String name, String elem, String on, String off, String yes, String no) {
		MAIN = main;
		ERROR = error;
		NAME = name;
		ELEM = elem;
		YES = yes;
		NO = no;
		ON = on;
		OFF = off;
	}
	
	public static void consoleLog(String module, String body) {
		String message = new StringBuilder("§b").append(module).append("> ").append("§6").append(body).toString();
		Bukkit.getConsoleSender().sendMessage(message);
	}

	public static void log(String module, String body) {
		String message = new StringBuilder(module).append("> ").append(body).toString();
		API.getInstance().getLogger().info(message);
	}

	public static String main(String module, String body) {
		return StringUtils.replace(F.MAIN + body, "%module%", module);
	}

	public static String error(String body) {
		return F.ERROR + body;
	}

	/**
	 * General Message
	 * 
	 * @param elem
	 * @return The formatted String
	 */
	public static String elem(String elem) {
		return StringUtils.replace(F.ELEM, "%var%", elem);
	}

	/**
	 * Spieler/Mob/Waffennamen
	 * 
	 * @param elem
	 * @return The formatted String
	 */
	public static String name(String elem) {
		return StringUtils.replace(F.NAME, "%var%", elem);
	}
	/**
	 * Returns On or Off
	 * 
	 * @param var
	 * @return The formatted String
	 */
	public static String oo(boolean var) {
		return var ? ON : OFF;
	}
	
	public static String ctf(boolean var, String e, String d) {
		return var ? e : d;
	}

	/**
	 * Gibt entweder Ja oder Nein als Text aus
	 * 
	 * @param var
	 * @return The formatted String
	 */
	public static String tf(boolean var) {
		return var ? YES : NO;
	}

	public static String getProgBar(int percent, String append) {
		StringBuilder bar = new StringBuilder("");

		for (int i = 0; i < 10; i++) {
			if (i < (percent / 10)) {
				bar.append("§2§l[]");
			} else if (i == (percent / 10)) {
				bar.append("§2§l[]");
			} else {
				bar.append("§2§l[]");
			}
		}
		bar.append("§e   " + append + "     ");
		return bar.toString();
	}

	/**
	 * Format a iterable Stringlist/Map to a readable String, separated with
	 * separator
	 * 
	 * @param objects
	 * @param separator
	 * @param ifEmpty
	 * @return a formated String, like player1, player2, player3, player4..
	 */
	public static String format(Iterable<?> objects, String separator, String ifEmpty) {
		if (!objects.iterator().hasNext()) {
			return ifEmpty;
		}
		StringBuilder sb = new StringBuilder();
		objects.spliterator().forEachRemaining(con -> sb.append(con.toString() + separator));
		return sb.toString().substring(0, (sb.toString().length() - separator.length()));
	}

	/**
	 * Gibt eine Anzahl der Herzen die der Spieler hat zurück.
	 * 
	 * @param health
	 * @param maxHealth
	 * @return The formatted String
	 */
	public static String getIconHealthString(int health, int maxHealth) {
		String filled = "";
		if (health > maxHealth) {
			health = maxHealth;
		}
		if (health % 2 != 0) {
			for (int filledHealth = 0; filledHealth < health / 2; filledHealth++) {
				filled = filled + "§4\u2764";
			}
			filled = filled + "§4\u2764";
			if (health + 1 < maxHealth) {
				for (int emptyHealth = 0; emptyHealth < (maxHealth - (health + 1)) / 2; emptyHealth++) {
					filled = filled +"§7\u2764";
				}
			}
		} else {
			for (int filledHealth = 0; filledHealth < health / 2; filledHealth++) {
				filled = filled +"§4\u2764";
			}
			if (health < maxHealth) {
				for (int emptyHealth = 0; emptyHealth < (maxHealth - health) / 2; emptyHealth++) {
					filled = filled  + "§7\u2764";
				}
			}
		}
		return filled;
	}
	
	public static String ProgressBar(double current, double max, int length, String fragment) {
		String bar = "";
		String full = ("§a" + fragment);
		String empty = ("§c" + fragment);
		double percent = (100 / max) * current;
		int fullfrags = (int) Math.round(percent / (100 / length));
		int emptyfrags = length - fullfrags;

		while (fullfrags > 0) {
			bar += full;
			fullfrags--;
		}
		while (emptyfrags > 0) {
			bar += empty;
			emptyfrags--;
		}

		return bar;
	}
}