package net.crytec.api.util;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.util.CraftMagicNumbers;

import lombok.experimental.UtilityClass;
import net.minecraft.server.v1_13_R2.BlockPosition;


@UtilityClass
public class UtilMap {

	/**
	 * Play a close Animation for a chest block
	 * 
	 * @param chest The specified ChestBlock
	 */
	public static void playChestCloseAnimation(Block chest) {
		Location loc = chest.getLocation();
		((CraftWorld) loc.getWorld()).getHandle().playBlockAction(new BlockPosition(chest.getX(), chest.getY(), chest.getZ()), CraftMagicNumbers.getBlock(chest.getType()), 1, 0);
	}

	/**
	 * Play a open Animation for a chest block
	 * 
	 * @param chest
	 */
	public static void playChestOpenAnimation(Block chest) {
		Location loc = chest.getLocation();
		((CraftWorld) loc.getWorld()).getHandle().playBlockAction(new BlockPosition(chest.getX(), chest.getY(), chest.getZ()), CraftMagicNumbers.getBlock(chest.getType()), 1, 1);
	}
}
