package net.crytec.api.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.UUID;
import java.util.stream.IntStream;

import org.apache.commons.codec.binary.Base64;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import lombok.Cleanup;
import lombok.experimental.UtilityClass;
import net.crytec.api.nbt.NBTItem;
import net.minecraft.server.v1_13_R2.MojangsonParser;
import net.minecraft.server.v1_13_R2.NBTTagCompound;

/**
 * This class offers methods for inventories and/or itemstacks.
 * 
 * @author crysis992
 *
 */
@UtilityClass
public class UtilInv {

	/**
	 * Insert an ItemStack to the players inventory. Also plays a sound for pickup.
	 * 
	 * @param player
	 * @param stack
	 */
	public static void insert(Player player, ItemStack stack) {
		insert(player, stack, false);
	}

	/**
	 * Insert an ItemStack to the players inventory. Also plays a sound for pickup.
	 * 
	 * @param player - The Player
	 * @param stack  - The Itemstack to insert
	 * @param drop   - Drop the item on the ground if the inventory is full.
	 */
	public static void insert(Player player, ItemStack stack, boolean drop) {
		insert(player, stack, drop, true);
	}
	
	/**
	 * Insert an ItemStack to the players inventory. Also plays a sound for pickup.
	 * 
	 * @param player - The Player
	 * @param stack  - The Itemstack to insert
	 * @param playsound - play pickup sound
	 * @param drop   - Drop the item on the ground if the inventory is full.
	 */
	public static void insert(Player player, final ItemStack stack, boolean drop, boolean playsound) {
		HashMap<Integer, ItemStack> items = player.getInventory().addItem(stack);
		
		if (items.size() == 0 && playsound) {
			UtilPlayer.playSound(player, Sound.ENTITY_ITEM_PICKUP);
		} else {
			items.values().forEach(itemLeft -> player.getWorld().dropItemNaturally(player.getLocation(), itemLeft));
			if (playsound) {
				player.getWorld().playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 0.8F, 1);
			}
		}
	}
	
	/**
	 * Insert an ItemStack to the players inventory. Also plays a sound for pickup.
	 * 
	 * @param player - The Player
	 * @param stack  - The Itemstack to insert
	 * @param playsound - play pickup sound
	 * @param drop   - Drop the item on the ground if the inventory is full.
	 */
	public static void insert(Inventory inventory, ItemStack stack, boolean drop, Location dropLocation) {
		inventory.addItem(stack).values().forEach(itemLeft -> {
			if(drop) {
				dropLocation.getWorld().dropItemNaturally(dropLocation, itemLeft);
			}
		});
	}

	/**
	 * Check if the inventory is empty
	 * 
	 * @param inv The Inventory to check
	 * @return -
	 */
	public static boolean isEmpty(Inventory inv) {
		return Arrays.stream(inv.getContents()).anyMatch(item -> item != null);
	}

	/**
	 * Check if the players Inventory contains at least a required amount of a specific material
	 * 
	 * @param player
	 * @param item     - The Material type
	 * @param required - The required amount
	 * @return -
	 */
	@Deprecated
	public static boolean contains(Player player, Material item, int required) {
		return player.getInventory().contains(item, required);
	}

	/**
	 * Remove a certain amount of Items with the given material type
	 * 
	 * @param player
	 * @param item
	 * @param toRemove
	 * @return -
	 */
	public static boolean remove(Player player, Material item, int toRemove) {
		if (!player.getInventory().contains(item, toRemove)) {
			return false;
		}

		for (Iterator<Integer> localIterator = player.getInventory().all(item).keySet().iterator(); localIterator.hasNext();) {
			int i = localIterator.next();

			if (toRemove > 0) {
				ItemStack stack = player.getInventory().getItem(i);

				if (stack.getData() == null) {
					int foundAmount = stack.getAmount();

					if (toRemove >= foundAmount) {
						toRemove -= foundAmount;
						player.getInventory().setItem(i, null);
					}

					else {
						stack.setAmount(foundAmount - toRemove);
						player.getInventory().setItem(i, stack);
						toRemove = 0;
					}
				}
			}
		}
		return true;
	}

	
	/**
	 * Drop the inventory content of a Inventory onto the ground
	 * @param dropLocation
	 * @param content
	 */
	public static void drop(Location dropLocation, ItemStack[] content) {
		drop(dropLocation, content, 0);
	}
	
	/**
	 * Drop the inventory content of a Inventory onto the ground
	 * @param dropLocation
	 * @param content
	 */
	public static void drop(Location dropLocation, ItemStack[] content, int pickupDelay) {
		
		World world = dropLocation.getWorld();
		
		for (ItemStack item : content) {
			if (item == null || item.getType() == Material.AIR) continue;
			Item drop = world.dropItemNaturally(dropLocation, item);
			if (pickupDelay > 0) {
				drop.setPickupDelay(pickupDelay);
			}
		}
	}


	/**
	 * Removes all Items with the given Material Type
	 * 
	 * @param player
	 * @param type
	 * @return -
	 */
	public static int removeAll(Player player, Material type) {
		HashSet<ItemStack> remove = new HashSet<ItemStack>();
		int count = 0;

		for (ItemStack item : player.getInventory().getContents()) {
			if ((item != null) && (item.getType() == type)) {
				count += item.getAmount();
				remove.add(item);
			}
		}
		for (ItemStack item : remove) {
			player.getInventory().remove(item);
		}
		return count;
	}

	/**
	 * Clear the players inventory. Optional with armor slots
	 * 
	 * @param player
	 * @param clearArmor
	 */
	public static void clear(Player player, boolean clearArmor) {
		for (int j = 0; j < player.getInventory().getSize(); j++) {
			player.getInventory().clear(j);
		}

		if (clearArmor) {
			player.getInventory().setArmorContents(new ItemStack[0]);
		}
		player.updateInventory();
	}

	public static boolean hasSpot(Player player) {
		int items = player.getInventory().firstEmpty();
		if (items == -1) {
			return false;
		}
		return true;
	}
	
	public static boolean hasSpot(Inventory inventory) {
		int items = inventory.firstEmpty();
		if (items == -1) {
			return false;
		}
		return true;
	}
	
	/**
	 * Checks if the inventory contains a specific amount of the given
	 * ItemStack.
	 * 
	 * @param player
	 * @param item
	 * @param amount
	 * @return
	 */
	public static boolean hasItems(Inventory inventory, ItemStack item, int amount) {
		
		int fullAmount = amount * item.getAmount();
		
		return fullAmount <= IntStream.range(0, inventory.getSize())
				.mapToObj(i -> inventory.getItem(i))
				.filter(i -> i != null && i.isSimilar(item))
				.mapToInt(i -> i.getAmount())
				.sum();
		
	}

	/**
	 * Remove a specific amount of items of the given type from an Inventory
	 * 
	 * @param inventory
	 * @param mat
	 * @param quantity
	 * @return -
	 */
	public static int removeItem(Inventory inventory, Material mat, int quantity) {
		ItemStack current;
		int currentAmount, left = quantity;
		
		for(int i = 0; i < inventory.getSize(); i++) {
			current = inventory.getItem(i);
			if(current == null || !current.getType().equals(mat)) continue;
			currentAmount = current.getAmount();
			if(currentAmount < left) {
				left -= currentAmount;
				inventory.clear(i);
				continue;
			} else {
				current.setAmount(current.getAmount() - left);
				return 0;
			}
		}
		
		return left;
	}
	
	public static int removeItem(Inventory inventory, ItemStack item, int amount) {
		
		int fullAmount = amount * item.getAmount();
		
		ItemStack current;
		int currentAmount, left = fullAmount;
		
		for(int i = 0; i < inventory.getSize(); i++) {
			current = inventory.getItem(i);
			if(current == null || !current.isSimilar(item)) continue;
			currentAmount = current.getAmount();
			if(currentAmount < left) {
				left -= currentAmount;
				inventory.clear(i);
				continue;
			}else {
				current.setAmount(current.getAmount() - left);
				return 0;
			}
		}
		
		return left;
	}

	/**
	 * Deserializes an ItemStack to an JSON String. Returns null if the given String is null or equals <b>"leer"</b>
	 * 
	 * @param json - The serialized JSON String in Mojangs format
	 * @return -
	 */
	public static ItemStack deserializeItemStack(String json) {
		if (json == null || json.equals("leer")) {
			return null;
		}
		try {
			NBTTagCompound comp = MojangsonParser.parse(json);
			net.minecraft.server.v1_13_R2.ItemStack cis = net.minecraft.server.v1_13_R2.ItemStack.a(comp);
			return CraftItemStack.asBukkitCopy(cis);
		} catch (CommandSyntaxException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Serializes an ItemStack into a JSON String
	 * 
	 * @param is
	 * @return -
	 */
	public static String serializeItemStack(ItemStack is) {
		if (is == null)
			return "leer";
		net.minecraft.server.v1_13_R2.ItemStack nms = CraftItemStack.asNMSCopy(is);
		NBTTagCompound tag = new NBTTagCompound();
		nms.save(tag);
		return tag.toString();
	}

	/**
	 * Serializes an ItemStack array to a Base64 encoded String
	 * 
	 * @param items
	 * @return -
	 * @throws IOException
	 */
	public static String itemStackArrayToBase64(ItemStack[] items) throws IOException {

		@Cleanup
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		@Cleanup
		BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

		// Write the size of the inventory
		dataOutput.writeInt(items.length);

		// Save every element in the list
		for (int i = 0; i < items.length; i++) {
			dataOutput.writeObject(items[i]);
		}

		// Serialize that array
		return Base64Coder.encodeLines(outputStream.toByteArray());
	}

	/**
	 * Deserializes an Base64 String to an ItemStack array.
	 * 
	 * @param data
	 * @return -
	 * @throws IOException
	 */
	public static ItemStack[] itemStackArrayFromBase64(String data) throws IOException {
		@Cleanup
		ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
		@Cleanup
		BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
		ItemStack[] items = new ItemStack[dataInput.readInt()];

		// Read the serialized inventory
		try {
			for (int i = 0; i < items.length; i++) {
				items[i] = (ItemStack) dataInput.readObject();
			}
		} catch (ClassNotFoundException ex) {
			throw new IOException("Unable to decode class type.", ex);
		}

		return items;
	}

	/**
	 * <b> WARNING </b><br>
	 * <b> UNSTABLE API </b>
	 * <p>
	 * Returns a Player skull with the applied Texture from the given URL. <br>
	 * The URL must be a valid Mojang texture URL
	 * 
	 * @param url - a valid mojang textureserver url
	 * @return -
	 */
	@Deprecated
	public static ItemStack getSkull(String url) {
		ItemStack head = new ItemStack(Material.PLAYER_HEAD);
		if (url.isEmpty())
			return head;

		SkullMeta headMeta = (SkullMeta) head.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);
		byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\"%s\"}}}", url).getBytes());
		profile.getProperties().put("textures", new Property("textures", new String(encodedData)));
		profile.getProperties().put("Signature", new Property("Signature", "Signature"));
		Field profileField = null;
		try {
			profileField = headMeta.getClass().getDeclaredField("profile");
			profileField.setAccessible(true);
			profileField.set(headMeta, profile);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		head.setItemMeta(headMeta);
		NBTItem nbt = new NBTItem(head);
		nbt.setString("Signature", "temp fix");
		return nbt.getItem();
	}
}
