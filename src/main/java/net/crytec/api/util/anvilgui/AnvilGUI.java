package net.crytec.api.util.anvilgui;

import java.util.function.BiFunction;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import lombok.Getter;
import net.crytec.API;

/**
 * An anvil gui, used for gathering a user's input
 * @author Wesley Smith
 * @since 1.0
 */
public class AnvilGUI {
	
    /**
     * The local {@link VersionWrapper} object for the server's version
     */
    private static AnvilImplementation WRAPPER = new AnvilImplementation();
    
    private static final AnvilGUIListener listener = new AnvilGUIListener();

    /**
     * The player who has the GUI open
     */
    @Getter
    private final Player holder;
    /**
     * The ItemStack that is in the {@link Slot#INPUT_LEFT} slot.
     */
    @Getter
    private final ItemStack insert;
    /**
     * Called when the player clicks the {@link Slot#OUTPUT} slot
     */
    @Getter
    private final BiFunction<Player, String, String> biFunction;

    /**
     * The container id of the inventory, used for NMS methods
     */
    private final int containerId;
    /**
     * The inventory that is used on the Bukkit side of things
     */
    @Getter
    private final Inventory inventory;

    /**
     * Represents the state of the inventory being open
     */
    @Getter
    private boolean open;


    /**
     * Create an AnvilGUI and open it for the player.
     * @param plugin A {@link org.bukkit.plugin.java.JavaPlugin} instance
     * @param holder The {@link Player} to open the inventory for
     * @param insert What to have the text already set to
     * @param biFunction A {@link BiFunction} that is called when the player clicks the {@link Slot#OUTPUT} slot
     * @throws NullPointerException If the server version isn't supported
     */
    public AnvilGUI(Plugin plugin, Player holder, String insert, BiFunction<Player, String, String> biFunction) {
        this.holder = holder;
        this.biFunction = biFunction;

        final ItemStack paper = new ItemStack(Material.PAPER);
        final ItemMeta paperMeta = paper.getItemMeta();
        paperMeta.setDisplayName(insert);
        paper.setItemMeta(paperMeta);
        this.insert = paper;

        WRAPPER.handleInventoryCloseEvent(holder);
        WRAPPER.setActiveContainerDefault(holder);

        final Object container = WRAPPER.newContainerAnvil(holder);

        inventory = WRAPPER.toBukkitInventory(container);
        inventory.setItem(Slot.INPUT_LEFT, this.insert);

        containerId = WRAPPER.getNextContainerId(holder);
        WRAPPER.sendPacketOpenWindow(holder, containerId);
        WRAPPER.setActiveContainer(holder, container);
        WRAPPER.setActiveContainerId(container, containerId);
        WRAPPER.addActiveContainerSlotListener(container, holder);

        open = true;
        listener.add(holder, this);
    }
    
    public AnvilGUI(Player holder, String insert, BiFunction<Player, String, String> biFunction) {
    	this(API.getInstance(), holder, insert, biFunction);
    }

    /**
     * Closes the inventory if it's open.
     * @throws IllegalArgumentException If the inventory isn't open
     */
    public void closeInventory() {
    	if (!open) {
    		listener.remove(holder);
    		return;
    	}
        open = false;

        WRAPPER.handleInventoryCloseEvent(holder);
        WRAPPER.setActiveContainerDefault(holder);
        WRAPPER.sendPacketCloseWindow(holder, containerId);
    }

    /**
     * Class wrapping the magic constants of slot numbers in an anvil GUI
     */
    public static class Slot {

        /**
         * The slot on the far left, where the first input is inserted. An {@link ItemStack} is always inserted
         * here to be renamed
         */
        public static final int INPUT_LEFT = 0;
        /**
         * Not used, but in a real anvil you are able to put the second item you want to combine here
         */
        public static final int INPUT_RIGHT = 1;
        /**
         * The output slot, where an item is put when two items are combined from {@link #INPUT_LEFT} and
         * {@link #INPUT_RIGHT} or {@link #INPUT_LEFT} is renamed
         */
        public static final int OUTPUT = 2;

    }

}