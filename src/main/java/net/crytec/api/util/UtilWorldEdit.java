package net.crytec.api.util;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.world.World;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.managers.storage.StorageException;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import lombok.experimental.UtilityClass;


@UtilityClass
public class UtilWorldEdit {
	
	
	/**
	 * Get the WorldGuard API instance, returns null if WorldGuard is not
	 * installed.
	 * 
	 * @return The WorldGuard Plugin instance
	 */
	public static WorldGuardPlugin getWorldGuard() {
		Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");

		if ((plugin == null) || (!(plugin instanceof WorldGuardPlugin))) {
			return null;
		}
		return (WorldGuardPlugin) plugin;
	}

	/**
	 * Returns a list with region names the player is currently in.
	 * 
	 * @param player
	 * @return The list of regions
	 */
	public static List<String> getCurrentRegions(Player player) {
		LocalPlayer lp = WorldGuardPlugin.inst().wrapPlayer(player);
		RegionManager rm = WorldGuard.getInstance().getPlatform().getRegionContainer().get(lp.getWorld());
		
		return rm.getApplicableRegionsIDs(lp.getLocation().toVector().toBlockPoint());
	}

	public static ProtectedRegion createWGRegion(int radius, Location midpoint, String regionname) {

		int x = midpoint.getBlockX();
		int z = midpoint.getBlockZ();
		int y = midpoint.getBlockY();
		World world = BukkitAdapter.adapt(midpoint.getWorld());
		RegionManager manager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(world);

		int half_size = Math.round(radius / 2);
		int _height = 256;
		int _depth = 0;
		
		BlockVector3 p1 = BlockVector3.at(x + half_size, y + _height, z + half_size);
		BlockVector3 p2 = BlockVector3.at(x - half_size, y - _depth, z - half_size);
		
		ProtectedRegion pr = null;
		pr = new ProtectedCuboidRegion(regionname, p1, p2);

		manager.addRegion(pr);
		try {
			manager.save();
			return pr;
		} catch (StorageException ex) {
			ex.printStackTrace();
			return null;
		}
	}
}