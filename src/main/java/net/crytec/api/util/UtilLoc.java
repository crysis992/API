package net.crytec.api.util;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import lombok.experimental.UtilityClass;
import net.crytec.API;


@UtilityClass
public class UtilLoc {

	private static final BlockFace[] axis = { BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST };
	private static final BlockFace[] radial = { BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST, BlockFace.SOUTH_EAST, BlockFace.SOUTH, BlockFace.SOUTH_WEST, BlockFace.WEST, BlockFace.NORTH_WEST };

	/**
	 * Convert the yaw value to a BlockFace direction.
	 * 
	 * @param yaw
	 * @param useSubCardinalDirections - Returns values like NORTH_EAST
	 * @return -
	 */
	public static BlockFace yawToFace(float yaw, boolean useSubCardinalDirections) {
		if (useSubCardinalDirections)
			return radial[Math.round(yaw / 45f) & 0x7].getOppositeFace();

		return axis[Math.round(yaw / 90f) & 0x3].getOppositeFace();
	}
	
	
	/**
	 * Check if the chunk at the given location is loaded.
	 * 
	 * @param location
	 * @return true if the chunk is loaded.
	 */
	public static boolean isChunkLoaded(Location location) {
		int chunkX = location.getBlockX() >> 4;
		int chunkZ = location.getBlockZ() >> 4;
		return location.getWorld().isChunkLoaded(chunkX, chunkZ);
	}

	/**
	 * Check if the player is in the radius of the given location without respecting the heigth of a player.
	 * 
	 * @param player
	 * @param loc
	 * @param radius
	 * @return -
	 */
	public static boolean isInRadius(Player player, Location loc, double radius) {
		double x = loc.getX() - player.getLocation().getX();
		double z = loc.getZ() - player.getLocation().getZ();
		double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(z, 2));
		if (distance <= radius)
			return true;
		else
			return false;
	}

	/**
	 * Check if the player is in the radius of the given location.
	 * 
	 * @param player
	 * @param loc
	 * @param radius
	 * @return -
	 */
	public static boolean isInRadius3D(Player player, Location loc, double radius) {
		double x = loc.getX() - player.getLocation().getX();
		double y = loc.getY() - player.getLocation().getY();
		double z = loc.getZ() - player.getLocation().getZ();
		double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
		if (distance <= radius)
			return true;
		else
			return false;
	}

	/**
	 * Check if the location midpoint is in the radius of the location
	 * 
	 * @param midpoint
	 * @param loc
	 * @param radius
	 * @return -
	 */
	public static boolean isInRadius3D(Location midpoint, Location loc, double radius) {
		double x = loc.getX() - midpoint.getX();
		double y = loc.getY() - midpoint.getY();
		double z = loc.getZ() - midpoint.getZ();
		double distance = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
		if (distance <= radius)
			return true;
		else
			return false;
	}

	/**
	 * Get a random location around the player
	 * 
	 * @param player
	 * @param XmaxDistance
	 * @param ZmaxDistance
	 * @return -
	 */
	public static Location getRandomLocationArroundPlayer(Player player, int XmaxDistance, int ZmaxDistance) {
		World world = player.getWorld();

		int randomX = 0;
		int randomZ = 0;

		double x = player.getLocation().getX();
		double y = 0.0D;
		double z = player.getLocation().getZ();

		randomX = 1 + (int) (Math.random() * (XmaxDistance - 1 + 1));
		randomZ = 1 + (int) (Math.random() * (ZmaxDistance - 1 + 1));

		x = x + randomX;
		y = player.getLocation().clone().add(0, 1, 0).getY();
		z = z + randomZ;
		return new Location(world, x, y, z);
	}

	/**
	 * Convert a location to a string that can be stored inside a database.
	 * 
	 * @param loc
	 * @return -
	 */
	public static String locToString(Location loc) {
		double x = loc.getBlockX();
		double y = loc.getBlockY();
		double z = loc.getBlockZ();
		float pitch = loc.getPitch();
		float yaw = loc.getYaw();
		String world = loc.getWorld().getName();

		String sep = ";";
		String compact = world + sep + x + sep + y + sep + z + sep + pitch + sep + yaw;
		compact = compact.replace('.', '_');
		return compact;
	}

	/**
	 * Convert a String that was stored with locToString() back to a location value
	 * 
	 * @param location
	 * @return -
	 */
	public static Location StringToLoc(String location) {
		location = location.replace('_', '.');
		String[] l = location.split(";");
		String world = l[0];
		double x = Double.parseDouble(l[1]);
		double y = Double.parseDouble(l[2]);
		double z = Double.parseDouble(l[3]);
		float pitch = Float.parseFloat(l[4]);
		float yaw = Float.parseFloat(l[5]);

		if (Bukkit.getWorld(world) == null) {
			API.getInstance().getLogger().severe("FAILED to parse location - World " + world + " is not loaded!");
			return null;
		}

		Location loc = new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);

		return loc;
	}

	/**
	 * Check if the given location is save.
	 * 
	 * If the given location is occupied or filled with fluid blocks, it will return false.
	 * 
	 * @param loc
	 * @return -
	 */
	public static boolean isSafe(Location loc) {

		World world = loc.getWorld();
		Block teleblock = world.getHighestBlockAt(loc).getRelative(BlockFace.DOWN);
		Block block1 = world.getBlockAt(loc.add(0, 1, 0));
		Block block2 = world.getBlockAt(loc.add(0, 2, 0));

		if (block1.isLiquid() || block2.isLiquid() || teleblock.isLiquid())
			return false;
		if (!teleblock.getType().isSolid() && block1.getType() == Material.AIR && block2.getType() == Material.AIR)
			return false;

		return true;
	}

	/**
	 * Get a random Location from the given world that's inside the worldborder
	 * 
	 * @param w
	 * @return -
	 */
	public static Location getRandomLoc(World w) {
		int centerX = (int) w.getWorldBorder().getCenter().getX();
		int centerZ = (int) w.getWorldBorder().getCenter().getZ();

		int maxX = centerX + (int) w.getWorldBorder().getSize() / 2;
		int maxZ = centerZ + (int) w.getWorldBorder().getSize() / 2;

		int minX = centerX - (int) w.getWorldBorder().getSize() / 2;
		int minZ = centerZ - (int) w.getWorldBorder().getSize() / 2;

		int randomX = UtilMath.getRandom(minX, maxX);
		int randomZ = UtilMath.getRandom(minZ, maxZ);

		return new Location(w, randomX, 0, randomZ);
	}

	public static String getLogString(Location loc) {
		return "(" + loc.getWorld().getName() + ")" + " X: " + UtilMath.trim(1, loc.getX()) + " Y: " + UtilMath.trim(1, loc.getY()) + " Z: " + UtilMath.trim(1, loc.getZ());
	}

    public static List<Player> getClosestPlayersFromLocation(final Location location, final double distance) {
        final List<Player> result = new ArrayList<Player>();
        final double d2 = distance * distance;
        for (final Player player : location.getWorld().getPlayers()) {
            if (player.getLocation().add(0.0, 0.85, 0.0).distanceSquared(location) <= d2) {
                result.add(player);
            }
        }
        return result;
    }
	
    public static List<Entity> getClosestEntitiesFromLocation(final Location location, final double radius) {
        final double chunkRadius = (radius < 16.0) ? 1.0 : ((radius - radius % 16.0) / 16.0);
        final List<Entity> radiusEntities = new ArrayList<Entity>();
        for (double chX = 0.0 - chunkRadius; chX <= chunkRadius; ++chX) {
            for (double chZ = 0.0 - chunkRadius; chZ <= chunkRadius; ++chZ) {
                final int x = (int)location.getX();
                final int y = (int)location.getY();
                final int z = (int)location.getZ();
                for (final Entity e : new Location(location.getWorld(), x + chX * 16.0, (double)y, z + chZ * 16.0).getChunk().getEntities()) {
                    if (e.getLocation().distance(location) <= radius && e.getLocation().getBlock() != location.getBlock() && e instanceof Entity) {
                        radiusEntities.add(e);
                    }
                }
            }
        }
        return radiusEntities;
    }

	/**
	 * Return the center of a given location/block
	 * @param loc
	 * @return -
	 */
	public static Location getCenter(Location loc) {
		return loc.clone().add(0.5, 0, 0.5);
	}
	
	/**
	 * A number of points in a circle (horizontal)
	 * @param center
	 * @param radius
	 * @param amount
	 * @return -
	 */
	public static ArrayList<Location> getCircle(Location center, double radius, int amount) {
		World world = center.getWorld();
		double increment = (2 * Math.PI) / amount;
		ArrayList<Location> locations = new ArrayList<Location>();
		for (int i = 0; i < amount; i++) {
			double angle = i * increment;
			double x = center.getX() + (radius * Math.cos(angle));
			double z = center.getZ() + (radius * Math.sin(angle));
			locations.add(new Location(world, x, center.getY(), z));
		}
		return locations;
	}
	
	/**
	 * Returns a target-oriented location (yaw / pitch) with a precision factor
	 * @param from
	 * @param to
	 * @param aiming 100 = 100% precision
	 * @return -
	 */
	public static Location lookAt(Location from, Location to, float aiming) {
		// Clone the loc to prevent applied changes to the input loc
		from = from.clone();

		// Values of change in distance (make it relative)
		double dx = to.getX() - from.getX();
		double dy = to.getY() - from.getY();
		double dz = to.getZ() - from.getZ();

		// Set yaw
		if (dx != 0) {
			// Set yaw start value based on dx
			if (dx < 0) {
				from.setYaw((float) (1.5 * Math.PI));
			}
			else {
				from.setYaw((float) (0.5 * Math.PI));
			}
			from.setYaw((float) from.getYaw() - (float) Math.atan(dz / dx));
		}
		else if (dz < 0) {
			from.setYaw((float) Math.PI);
		}

		// Get the distance from dx/dz
		double dxz = Math.sqrt(Math.pow(dx, 2) + Math.pow(dz, 2));

		// Set pitch
		from.setPitch((float) -Math.atan(dy / dxz));

		// Yaw
		float yawAiming = 360 - (aiming * 360 / 100);
		if (yawAiming > 0) yawAiming = UtilMath.getRandom((int) yawAiming, (int) -yawAiming);
		// Pitch
		float pitchAiming = 180 - (aiming * 180 / 100);
		if (pitchAiming > 0) pitchAiming = UtilMath.getRandom((int) pitchAiming, (int) -pitchAiming);

		// Set values, convert to degrees (invert the yaw since Bukkit uses a different yaw dimension format)
		from.setYaw(-from.getYaw() * 180f / (float) Math.PI + yawAiming);
		from.setPitch(from.getPitch() * 180f / (float) Math.PI + pitchAiming);
		return from;
	}
}
