package net.crytec.api.util;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import co.aikar.taskchain.TaskChain;
import co.aikar.taskchain.TaskChainAbortAction;

public class TaskChainError implements TaskChainAbortAction<Player, String, Sound> {
	
	@Override
	public void onAbort(TaskChain<?> chain, Player player, String message) {
		player.sendMessage(message);
	}
	
	@Override
	public void onAbort(TaskChain<?> chain, Player player) {
		TaskChainAbortAction.super.onAbort(chain, player);
	}
	
	@Override
	public void onAbort(TaskChain<?> chain, Player player, String message, Sound sound) {
		player.sendMessage(message);
		player.playSound(player.getLocation(), sound, 1, 1);
	}

}
