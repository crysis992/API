package net.crytec.api.util;

import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.crytec.API;

public class ChatStringInput implements Listener {

	private static HashMap<UUID, Consumer<String>> players = new HashMap<UUID, Consumer<String>>();
	private static boolean initialized;
	
	public static void initialize() {
		if (!initialized) new ChatStringInput();
		else throw new IllegalAccessError("This class cannot be instantiated.");
	}
	
	private ChatStringInput() {
		Bukkit.getPluginManager().registerEvents(this, API.getInstance());
		ChatStringInput.initialized = true;
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event) {
		if (players.containsKey(event.getPlayer().getUniqueId())) {
			Consumer<String> result = players.get(event.getPlayer().getUniqueId());
			players.remove(event.getPlayer().getUniqueId());
			result.accept(event.getMessage());
			event.setCancelled(true);
			return;
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (players.containsKey(e.getPlayer().getUniqueId())) {
			players.remove(e.getPlayer().getUniqueId());
		}
	}

	/**
	 * Registers a player, the {@link Consumer} is waiting for the next String
	 * the player types into their chat and accepts the result.
	 * 
	 * @param player
	 *            - The player to register
	 * @param result
	 *            - The players input
	 */
	public static void addPlayer(Player player, Consumer<String> result) {
		players.put(player.getUniqueId(), result);
	}
}
