package net.crytec.api.util;

import java.util.Collection;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class MaterialSet implements Tag<Material> {

    private final NamespacedKey key;
    private final Set<Material> materials;

    public MaterialSet(@NotNull NamespacedKey key, @NotNull Predicate<Material> filter) {
        this(key, Stream.of(Material.values()).filter(filter).collect(Collectors.toList()));
    }

    public MaterialSet(@NotNull NamespacedKey key, @NotNull Material... materials) {
        this(key, Lists.newArrayList(materials));
    }

    public MaterialSet(@NotNull NamespacedKey key, @NotNull Collection<Material> materials) {
        this.key = key;
        this.materials = Sets.newEnumSet(materials, Material.class);
    }

    @NotNull
    @Override
    public NamespacedKey getKey() {
        return key;
    }

    @SuppressWarnings("unchecked")
	@NotNull
    public MaterialSet add(@NotNull Tag<Material>... tags) {
        for (Tag<Material> tag : tags) {
            add(tag.getValues());
        }
        return this;
    }

    @NotNull
    public MaterialSet add(@NotNull MaterialSet... tags) {
        for (Tag<Material> tag : tags) {
            add(tag.getValues());
        }
        return this;
    }

    @NotNull
    public MaterialSet add(@NotNull Material... material) {
        this.materials.addAll(Lists.newArrayList(material));
        return this;
    }

    @NotNull
    public MaterialSet add(@NotNull Collection<Material> materials) {
        this.materials.addAll(materials);
        return this;
    }

    @NotNull
    public MaterialSet contains(@NotNull String with) {
        return add(mat -> mat.name().contains(with));
    }

    @NotNull
    public MaterialSet endsWith(@NotNull String with) {
        return add(mat -> mat.name().endsWith(with));
    }


    @NotNull
    public MaterialSet startsWith(@NotNull String with) {
        return add(mat -> mat.name().startsWith(with));
    }
    @SuppressWarnings("deprecation")
	@NotNull
    public MaterialSet add(@NotNull Predicate<Material> filter) {
        add(Stream.of(Material.values()).filter(((Predicate<Material>) Material::isLegacy).negate()).filter(filter).collect(Collectors.toList()));
        return this;
    }

    @NotNull
    public MaterialSet not(@NotNull MaterialSet tags) {
        not(tags.getValues());
        return this;
    }

    @NotNull
    public MaterialSet not(@NotNull Material... material) {
        this.materials.removeAll(Lists.newArrayList(material));
        return this;
    }

    @NotNull
    public MaterialSet not(@NotNull Collection<Material> materials) {
        this.materials.removeAll(materials);
        return this;
    }

    @SuppressWarnings("deprecation")
	@NotNull
    public MaterialSet not(@NotNull Predicate<Material> filter) {
        not(Stream.of(Material.values()).filter(((Predicate<Material>) Material::isLegacy).negate()).filter(filter).collect(Collectors.toList()));
        return this;
    }

    @NotNull
    public MaterialSet notEndsWith(@NotNull String with) {
        return not(mat -> mat.name().endsWith(with));
    }


    @NotNull
    public MaterialSet notStartsWith(@NotNull String with) {
        return not(mat -> mat.name().startsWith(with));
    }

    @NotNull
    public Set<Material> getValues() {
        return this.materials;
    }

    public boolean isTagged(@NotNull BlockData block) {
        return isTagged(block.getMaterial());
    }

    public boolean isTagged(@NotNull BlockState block) {
        return isTagged(block.getType());
    }

    public boolean isTagged(@NotNull Block block) {
        return isTagged(block.getType());
    }

    public boolean isTagged(@NotNull ItemStack item) {
        return isTagged(item.getType());
    }

    public boolean isTagged(@NotNull Material material) {
        return this.materials.contains(material);
    }

    @SuppressWarnings("deprecation")
	@NotNull
    public MaterialSet ensureSize(@NotNull String label, int size) {
        long actual = this.materials.stream().filter(((Predicate<Material>) Material::isLegacy).negate()).count();
        if (size != actual) {
            throw new IllegalStateException(label + " - Expected " + size + " materials, got " + actual);
        }
        return this;
    }
}
