package net.crytec.api.util;

import org.bukkit.Bukkit;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UtilServer {

	/**
	 * Broadcast a message to a specific Rank
	 * 
	 * @param message
	 * @param permission
	 *            - The given permission
	 */
	public static void broadcast(String message, String permission) {
		broadcast(null, message, permission);
	}

	/**
	 * Broadcast a message to a specific Rank
	 * 
	 * @param prefix
	 *            The prefix
	 * @param message
	 *            The Message
	 * @param permission
	 */
	public static void broadcast(String prefix, String message, String permission) {
		Bukkit.getOnlinePlayers().stream().filter(p -> p.hasPermission(permission)).forEach(cur -> cur.sendMessage(F.main((prefix == null) ? "Info" : prefix, message)));
	}
}
