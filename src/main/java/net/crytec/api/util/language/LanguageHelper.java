package net.crytec.api.util.language;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import net.crytec.api.util.language.convert.EnumEnchantment;
import net.crytec.api.util.language.convert.EnumEnchantmentLevel;
import net.crytec.api.util.language.convert.EnumEntity;
import net.crytec.api.util.language.convert.EnumItem;
import net.crytec.api.util.language.convert.EnumLang;
import net.crytec.api.util.language.convert.EnumPotionEffect;

public class LanguageHelper {
	
	
	public static EnumLang DEFAULT_LANGUAGE = EnumLang.EN_US;
	

	/**
	 * Return the display name of the item.
	 *
	 * @param item   The item
	 * @return The name of the item
	 */
	public static String getItemDisplayName(ItemStack item) {
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName())
			return item.getItemMeta().getDisplayName();
		else
			return getItemName(item);
	}
	
	/**
	 * Return the localized name of the item.
	 *
	 * @param item   The item
	 * @return The localized name. if the item doesn't have a localized name, this method will return the unlocalized name of it.
	 */
	public static String getItemName(ItemStack item) {
		// Potion & SpawnEgg & Player Skull
		if (item.getType() == Material.POTION || item.getType() == Material.SPLASH_POTION || item.getType() == Material.LINGERING_POTION || item.getType() == Material.TIPPED_ARROW)
			return EnumPotionEffect.getLocalizedName(item, DEFAULT_LANGUAGE);
		else if (item.getType() == Material.PLAYER_HEAD || item.getType() == Material.PLAYER_WALL_HEAD) // is player's skull
			return EnumItem.getPlayerSkullName(item, DEFAULT_LANGUAGE);

		return translateToLocal(getItemUnlocalizedName(item));
	}

	public static String getMaterialName(Material material) {
		return translateToLocal(getItemUnlocalizedName(material));
	}

	/**
	 * Return the unlocalized name of the item(Minecraft convention)
	 *
	 * @param item The item
	 * @return The unlocalized name. If the item doesn't have a unlocalized name, this method will return the Material of it.
	 */
	public static String getItemUnlocalizedName(ItemStack item) {
		EnumItem enumItem = EnumItem.get(item.getType());
		return enumItem != null ? enumItem.getUnlocalizedName() : item.getType().toString();
	}

	/**
	 * Return the unlocalized name of the item(Minecraft convention)
	 *
	 * @param material The item
	 * @return The unlocalized name. If the item doesn't have a unlocalized name, this method will return the Material of it.
	 */
	public static String getItemUnlocalizedName(Material material) {
		EnumItem enumItem = EnumItem.get(material);
		return enumItem != null ? enumItem.getUnlocalizedName() : material.toString();
	}

	/**
	 * Return the unlocalized name of the entity(Minecraft convention)
	 *
	 * @param entity The entity
	 * @return The unlocalized name. If the entity doesn't have a unlocalized name, this method will return the EntityType of it.
	 */
	public static String getEntityUnlocalizedName(Entity entity) {
		EnumEntity enumEntity = EnumEntity.get(entity.getType());
		return enumEntity != null ? enumEntity.getUnlocalizedName() : entity.getType().toString();
	}

	/**
	 * Return the unlocalized name of the entity(Minecraft convention)
	 *
	 * @param entityType The EntityType of the entity
	 * @return The unlocalized name. If the entity doesn't have a unlocalized name, this method will return the name of the EntityType.
	 */
	public static String getEntityUnlocalizedName(EntityType entityType) {
		EnumEntity enumEntity = EnumEntity.get(entityType);
		return enumEntity != null ? enumEntity.getUnlocalizedName() : entityType.toString();
	}

	/**
	 * Return the display name of the entity.
	 *
	 * @param entity The entity
	 * @return The name of the entity
	 */
	public static String getEntityDisplayName(Entity entity) {
		return entity.getCustomName() != null ? entity.getCustomName() : getEntityName(entity);
	}

	/**
	 * Return the localized name of the entity.
	 *
	 * @param entity The entity
	 * @return The localized name. if the entity doesn't have a localized name, this method will return the unlocalized name of it.
	 */
	public static String getEntityName(Entity entity) {
		return translateToLocal(getEntityUnlocalizedName(entity));
	}

	/**
	 * Return the localized name of the entity.
	 *
	 * @param entityType The EntityType of the entity
	 * @return The localized name. if the entity doesn't have a localized name, this method will return the unlocalized name of it.
	 */
	public static String getEntityName(EntityType entityType) {
		return translateToLocal(getEntityUnlocalizedName(entityType));
	}

	/**
	 * Return the unlocalized name of the enchantment level(Minecraft convention)
	 *
	 * @param level The enchantment level
	 * @return The unlocalized name.(if level is greater than 10, it will only return the number of the level)
	 */
	public static String getEnchantmentLevelUnlocalizedName(int level) {
		EnumEnchantmentLevel enumEnchLevel = EnumEnchantmentLevel.get(level);
		return enumEnchLevel != null ? enumEnchLevel.getUnlocalizedName() : Integer.toString(level);
	}

	/**
	 * Return the name of the enchantment level
	 *
	 * @param level  The enchantment level
	 * @return The name of the level.(if level is greater than 10, it will only return the number of the level)
	 */
	public static String getEnchantmentLevelName(int level) {
		return translateToLocal(getEnchantmentLevelUnlocalizedName(level));
	}

	/**
	 * Return the unlocalized name of the enchantment(Minecraft convention)
	 *
	 * @param enchantment The enchantment
	 * @return The unlocalized name.
	 */
	public static String getEnchantmentUnlocalizedName(Enchantment enchantment) {
		EnumEnchantment enumEnch = EnumEnchantment.get(enchantment);
		return (enumEnch != null ? enumEnch.getUnlocalizedName() : enchantment.getKey().getKey());
	}

	/**
	 * Return the name of the enchantment.
	 *
	 * @param enchantment The enchantment
	 * @return The name of the enchantment
	 */
	public static String getEnchantmentName(Enchantment enchantment) {
		return translateToLocal(getEnchantmentUnlocalizedName(enchantment));
	}

	/**
	 * Return the display name of the enchantment(with level).
	 *
	 * @param enchantment The enchantment
	 * @param level       The enchantment level
	 * @return The name of the item
	 */
	public static String getEnchantmentDisplayName(Enchantment enchantment, int level) {
		String name = getEnchantmentName(enchantment);
		String enchLevel = getEnchantmentLevelName(level);
		return name + (enchLevel.length() > 0 ? " " + enchLevel : "");
	}

	/**
	 * Return the display name of the enchantment(with level).
	 *
	 * @param entry  The Entry of an enchantment with level The type is {@code Map.Entry<Enchantment, Integer>}
	 * @return The name of the item
	 */
	public static String getEnchantmentDisplayName(Map.Entry<Enchantment, Integer> entry) {
		return getEnchantmentDisplayName(entry.getKey(), entry.getValue());
	}

	/**
	 * Translate unlocalized entry to localized entry.
	 *
	 * @param unlocalizedName The unlocalized entry.
	 * @return The localized entry. If the localized entry doesn't exist, it will first look up the fallback language map. If the entry still doesn't
	 *         exist, then return the unlocalized name.
	 */
	public static String translateToLocal(String unlocalizedName) {
		String result = DEFAULT_LANGUAGE.getMap().get(unlocalizedName);		
		if (result != null) 
			return result;
		else {
			result = EnumLang.EN_US.getMap().get(unlocalizedName);
		}		
		return result == null ? unlocalizedName : result;
	}
}