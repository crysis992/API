package net.crytec.api.WGRegionEvents;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.vehicle.VehicleMoveEvent;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;

import net.crytec.API;
import net.crytec.api.events.RegionEnterEvent;
import net.crytec.api.events.RegionEnteredEvent;
import net.crytec.api.events.RegionLeaveEvent;
import net.crytec.api.events.RegionLeftEvent;

public class WGRegionEventsListener implements Listener {
	private API plugin;

	private Map<Player, Set<ProtectedRegion>> playerRegions;
	
	private final RegionContainer container;
	
	private static boolean initialized;
	
	public static void initialize() {
		if (!initialized) new WGRegionEventsListener(API.getInstance());
		else throw new UnsupportedOperationException("You are not allowed to instantiate this class!");
	}

	private WGRegionEventsListener(API plugin) {
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, plugin);
		this.container = WorldGuard.getInstance().getPlatform().getRegionContainer();
		playerRegions = Maps.newHashMap();
		
		initialized = true;
	}

	@EventHandler
	public void onPlayerKick(PlayerKickEvent e) {
		Set<ProtectedRegion> regions = playerRegions.remove(e.getPlayer());
		if (regions != null) {
			for (ProtectedRegion region : regions) {
				RegionLeaveEvent leaveEvent = new RegionLeaveEvent(region, e.getPlayer(), MovementWay.DISCONNECT, e);
				RegionLeftEvent leftEvent = new RegionLeftEvent(region, e.getPlayer(), MovementWay.DISCONNECT, e);

				plugin.getServer().getPluginManager().callEvent(leaveEvent);
				plugin.getServer().getPluginManager().callEvent(leftEvent);
			}
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e) {
		Set<ProtectedRegion> regions = playerRegions.remove(e.getPlayer());
		if (regions != null) {
			for (ProtectedRegion region : regions) {
				RegionLeaveEvent leaveEvent = new RegionLeaveEvent(region, e.getPlayer(), MovementWay.DISCONNECT, e);
				RegionLeftEvent leftEvent = new RegionLeftEvent(region, e.getPlayer(), MovementWay.DISCONNECT, e);

				plugin.getServer().getPluginManager().callEvent(leaveEvent);
				plugin.getServer().getPluginManager().callEvent(leftEvent);
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onPlayerMove(PlayerMoveEvent e) {
		e.setCancelled(updateRegions(e.getPlayer(), MovementWay.MOVE, e.getTo(), e));
	}
	
	@EventHandler
	public void onPlayerChangeWorlds(PlayerChangedWorldEvent event) {
		clearRegions(event.getPlayer(), MovementWay.WORLD_CHANGE, event);
		updateRegions(event.getPlayer(), MovementWay.WORLD_CHANGE, event.getPlayer().getLocation(), event);
	}

	@EventHandler
	public void onPlayerTeleport(PlayerTeleportEvent event) {
		TeleportCause cause = event.getCause();
		MovementWay movementType = MovementWay.TELEPORT;
		if (cause == TeleportCause.END_PORTAL || cause == TeleportCause.NETHER_PORTAL) {
			clearRegions(event.getPlayer(), MovementWay.WORLD_CHANGE, event);
			movementType = MovementWay.WORLD_CHANGE;
		}
		updateRegions(event.getPlayer(), movementType, event.getTo(), event);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		updateRegions(e.getPlayer(), MovementWay.SPAWN, e.getPlayer().getLocation(), e);
	}

	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e) {
		updateRegions(e.getPlayer(), MovementWay.SPAWN, e.getRespawnLocation(), e);
	}
	
	@EventHandler
	public void onVehicleMove(VehicleMoveEvent event) {
		if (event.getVehicle().getPassengers() == null) return;
		List<Entity> passengers = event.getVehicle().getPassengers();
		
		passengers.stream()
		.filter(e -> (e instanceof Player) )
		.forEach(player -> updateRegions( (Player) player, MovementWay.RIDE, player.getLocation(), event));

	}
	
	
	private void clearRegions(Player player, MovementWay movementway, PlayerEvent event) {
		if (!this.playerRegions.containsKey(player)) return;
		
		for (ProtectedRegion region : playerRegions.get(player)) {
			
			RegionLeaveEvent LeaveEvent = new RegionLeaveEvent(region, player, movementway, event);
			RegionLeftEvent leftEvent = new RegionLeftEvent(region, player, movementway, event);
			
			Bukkit.getPluginManager().callEvent(LeaveEvent);
			Bukkit.getPluginManager().callEvent(leftEvent);
			
		}
		
		this.playerRegions.put(player, Sets.newHashSet());
	}

	private synchronized boolean updateRegions(final Player player, final MovementWay movement, Location to, final Event event) {
		Set<ProtectedRegion> regions;
		Set<ProtectedRegion> oldRegions;

		com.sk89q.worldedit.util.Location Wto = BukkitAdapter.adapt(to);
		
		if (playerRegions.get(player) == null) {
			regions = new HashSet<ProtectedRegion>();
		} else {
			regions = new HashSet<ProtectedRegion>(playerRegions.get(player));
		}

		oldRegions = new HashSet<ProtectedRegion>(regions);

		RegionManager rm = this.container.get(BukkitAdapter.adapt(to.getWorld()));

		if (rm == null) {
			return false;
		}
		
		HashSet<ProtectedRegion> appRegions = new HashSet<ProtectedRegion>(rm.getApplicableRegions(Wto.toVector().toBlockPoint()).getRegions());
		ProtectedRegion globalRegion = rm.getRegion("__global__");
		
		if (globalRegion != null) {
			appRegions.add(globalRegion);
		}

		for (final ProtectedRegion region : appRegions) {
			if (!regions.contains(region)) {
				RegionEnterEvent e = new RegionEnterEvent(region, player, movement, event);
				plugin.getServer().getPluginManager().callEvent(e);

				if (e.isCancelled()) {
					regions.clear();
					regions.addAll(oldRegions);

					return true;
				} else {
					
					Bukkit.getScheduler().runTaskLater(plugin, () ->
						plugin.getServer().getPluginManager().callEvent(new RegionEnteredEvent(region, player, movement, event))
					, 1L);
					regions.add(region);
				}
			}
		}

		Iterator<ProtectedRegion> itr = regions.iterator();
		
		while (itr.hasNext()) {
			final ProtectedRegion region = itr.next();
			if (!appRegions.contains(region)) {
				if (rm.getRegion(region.getId()) != region) {
					itr.remove();
					continue;
				}
				RegionLeaveEvent e = new RegionLeaveEvent(region, player, movement, event);

				plugin.getServer().getPluginManager().callEvent(e);

				if (e.isCancelled()) {
					regions.clear();
					regions.addAll(oldRegions);
					return true;
				} else {
					Bukkit.getScheduler().runTaskLater(plugin, () -> 
					plugin.getServer().getPluginManager().callEvent(new RegionLeftEvent(region, player, movement, event))
					, 1L);
					itr.remove();
				}
			}
		}
		
		playerRegions.put(player, regions);
		return false;
	}
}