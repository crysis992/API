package net.crytec.api.actionbar;

import lombok.Getter;
import lombok.Setter;

public class ActionSpreadBar {
	
	public ActionSpreadBar(){
		this("");
	}
	
	public ActionSpreadBar(String actionBar){
		this.actionBar = actionBar;
		this.showing = false;
	}
	
	@Getter @Setter
	private boolean showing;
	@Getter @Setter
	private String actionBar;
	
	public boolean toggle() {
		this.showing = !this.showing;
		return this.showing;
	}
	
}
