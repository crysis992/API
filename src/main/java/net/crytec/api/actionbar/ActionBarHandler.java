package net.crytec.api.actionbar;

import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Maps;

import net.crytec.API;
import net.crytec.api.actionbar.ActionBarSection.StringArrangement;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
/**
 * 
 * @author Gestankbratwurst
 * @author crysis992
 *
 */
public class ActionBarHandler implements Listener {
	
	private static ActionBarHandler instance;
	
	public static ActionBarHandler get() {
		return instance;
	}
	
	private ConcurrentMap<Player, ActionBarContainer> container = Maps.newConcurrentMap();
	private ConcurrentMap<UUID, ActionBarSection> sections = Maps.newConcurrentMap();
	
	public ActionBarSection getSection(UUID id) {
		return sections.get(id);
	}
	
	public void removeSection(ActionBarSection section) {
		this.removeSection(section.getId());
	}
	
	public void removeSection(UUID id) {
		this.sections.remove(id);
	}
	
	public ActionBarSection createSection(int length, StringArrangement arrangement, int priority) {
		UUID id = UUID.randomUUID();
		ActionBarSection section = new ActionBarSection(length, arrangement, priority, id);
		this.sections.put(id, section);
		return section;
	}
	
	public void insertSection(Player player, int index, ActionBarSection section){
		this.container.get(player).insert(index, section);
	}
	
	public ActionBarHandler() {
		ActionBarHandler.instance = this;
		Bukkit.getPluginManager().registerEvents(this, API.getInstance());
		Bukkit.getScheduler().runTaskTimerAsynchronously(API.getInstance(), () -> {

			container.forEach((p, c) -> {
				p.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(c.getFullHighestPriorityBar()));
			});

		}, 45, 10);

	}
	
	public void updateFor(Player player) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(this.container.get(player).getFullHighestPriorityBar()));

	}
	
	public ActionBarContainer getActionBarContainer(Player player) {
		return container.get(player);
	}
	
	public ActionBarContainer introducePlayer(Player player, int sectionCount, int sectionLength, String sectionBorder) {
		ActionBarContainer container = new ActionBarContainer(sectionCount, sectionLength, sectionBorder);
		this.container.put(player, container);
		return container;
	}
	
	
	@EventHandler
	public void cleanOnQuit(PlayerQuitEvent event) {
		ActionBarContainer container = this.container.remove(event.getPlayer());
		if (container != null) container.cleanUp();
	}
}