package net.crytec.api.spigetupdater.core;

public class ResourceVersion {

	public int    id;
	public String name;
	public long   releaseDate;
	public String url;

}
