package net.crytec.api;

import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import net.crytec.API;

/**
 * Used to check if server runs Spigot or Paper fork. Used to get the current
 * server version.
 * 
 * @author crysis992
 *
 */
public class Versions {
	
	/**
	 * The currents server {@link Fork}
	 */
	@Getter
	private static Fork fork;
	
	/**
	 * The currents server {@link ServerVersion}
	 */
	@Getter
	private static ServerVersion version; 
	
	private static boolean isInit = false;
	
	/**
	 * -
	 */
	public static void initialize() {
		if(!isInit) new Versions();
	}
	
	private Versions() {
		if (Bukkit.getVersion().contains("Paper")) {
			Versions.fork = Fork.PAPER;
		} else {
			Versions.fork = Fork.SPIGOT;
		}
		String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
		
		if (EnumUtils.isValidEnum(ServerVersion.class, version)) {
			Versions.version = ServerVersion.valueOf(version);
		} else {
			Versions.version = ServerVersion.UNKNOWN;
		}
		
		Bukkit.getConsoleSender().sendMessage("§2Version Support: §6" + version + "§2 Fork: §6" + fork);
		isInit = true;
	}
	
	public static boolean isPaper() {
		return fork == Fork.PAPER;
	}
	
	/**
	 * The Server fork.
	 * @author crysis992
	 *
	 */
	public enum Fork {
		SPIGOT,
		PAPER
	}
	
	/**
	 * Used to check the version of {@link API}
	 * @param plugin the plugin that needs a version.
	 * @param requiredVersion the version that must be at least installed.
	 * @return if the current version is sufficient.
	 */
	public static boolean requireAPIVersion(JavaPlugin plugin, int requiredVersion) {
		String version = API.getInstance().getDescription().getVersion();
		version = version.replaceAll("\\.", "");
		int api_version = Integer.parseInt(version);
		if (api_version < requiredVersion) {
			String ver = String.valueOf(requiredVersion);
			log("===================================");
			log("§c Unable to load " + plugin.getDescription().getName());
			log("§c You need at least version: §e" + ver.substring(0, 1) + "." + ver.substring(1, 2) + "."
					+ ver.substring(2, 3) + "§c but you have: §6" + version);
			log("§c Please update CT-Core to the latest version if you wish to use this plugin!");
			log("§6 Download the latest version here: https://www.spigotmc.org/resources/24842/");
			log("===================================");
			return false;
		}
		return true;
	}
	
	/**
	 * Spigot server versions.
	 * @author crysis992
	 *
	 */
	public enum ServerVersion {
		
		v1_12_R1,
		v1_12_R2,
		v1_13_R1,
		v1_13_R2,
		UNKNOWN
	}
	
	private static void log(String message) {
		ConsoleCommandSender s = Bukkit.getConsoleSender();
		s.sendMessage(message);
	}
	
}