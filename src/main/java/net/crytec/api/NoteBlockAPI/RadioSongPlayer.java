package net.crytec.api.NoteBlockAPI;

import org.bukkit.SoundCategory;
import org.bukkit.entity.Player;

import net.crytec.API;

public class RadioSongPlayer extends SongPlayer {

	public RadioSongPlayer(Song song) {
		super(song);
	}

	@Override
	public void playTick(Player p, int tick) {
		byte playerVolume = API.getInstance().getNoteBlockPlayerManager().getPlayerVolume(p);

		for (Layer l : song.getLayerHashMap().values()) {
			Note note = l.getNote(tick);
			if (note == null) {
				continue;
			}
			p.playSound(p.getEyeLocation(), Instrument.getInstrument(note.getInstrument()), SoundCategory.RECORDS, (l.getVolume() * (int) volume * (int) playerVolume) / 1000000f,
					NotePitch.getPitch(note.getKey() - 33));
		}
	}
}
