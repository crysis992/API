package net.crytec.api.redis;

import org.bukkit.Bukkit;

import com.google.gson.JsonObject;

import net.crytec.api.redis.RedisManager.ServerType;

public class ServernameRequestPacket implements RedisPacketProvider {
	
	private static RedisManager manager;
	
	public ServernameRequestPacket(RedisManager manager) {
		ServernameRequestPacket.manager = manager;
	}
	
	public ServernameRequestPacket() { }

	@Override
	public String getID() {
		return "servername";
	}

	@Override
	public ServerType getReceiverType() {
		return ServerType.BUNGEE;
	}

	@Override
	public JsonObject getPacketData() {
		JsonObject obj = new JsonObject();
		obj.addProperty("servername", "requested");
		obj.addProperty("port", Bukkit.getServer().getPort());
		return obj;
	}

	@Override
	public void handlePacketReceive(JsonObject data) {
		String servername = data.get("servername").getAsString();
		int port = data.get("port").getAsInt();

		if (Bukkit.getServer().getPort() == port) {
			Bukkit.getLogger().info("Received ID from network. This server is now registered as: " + servername);
			manager.setServer(servername);
		}

	}

}
