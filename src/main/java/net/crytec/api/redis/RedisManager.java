package net.crytec.api.redis;

import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang3.Validate;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import lombok.Getter;
import net.crytec.IMulti;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class RedisManager {
	private IMulti plugin;
	
    private static final String CHANNEL = "crytec:update";
    @Getter
    private String server;

    private JedisPool jedisPool;
    private Subscription sub;
    private Gson gson = new Gson();
    
    private HashMap<String, RedisPacketProvider> registeredProviders = Maps.newHashMap();

    public RedisManager(IMulti plugin) {
    	this.plugin = plugin;
    	this.setServer(UUID.randomUUID().toString().replace("-", "").substring(0, 8));
    	this.init("localhost", "");
    }
    
    public void setServer(String id) {
    	this.server = id;
    }
    
    public void requestServername() {
    	  this.sendOutgoingMessage(new ServernameRequestPacket());
    }
    
	public void registerProvider(RedisPacketProvider provider) {
		Validate.isTrue(!this.registeredProviders.containsKey(provider.getID()), "Provider " + provider.getID() + " is already registered!");
		this.registeredProviders.put(provider.getID(), provider);
	}

    public void init(String address, String password) {
        String[] addressSplit = address.split(":");
        String host = addressSplit[0];
        int port = addressSplit.length > 1 ? Integer.parseInt(addressSplit[1]) : 6379;

        if (password.equals("")) {
            this.jedisPool = new JedisPool(new JedisPoolConfig(), host, port);
        } else {
            this.jedisPool = new JedisPool(new JedisPoolConfig(), host, port, 0, password);
        }
        
        plugin.runAsync( () -> {
        	this.sub = new Subscription(this);
        	try (Jedis jedis = this.jedisPool.getResource()) {
                jedis.subscribe(this.sub, CHANNEL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
    
    public void handleIncomingMessage(String msg) {
		JsonElement element = gson.fromJson (msg, JsonElement.class);
		JsonObject object = element.getAsJsonObject();
		
		String server = object.get("server").getAsString();
		String key = object.get("key").getAsString();
		ServerType type = ServerType.valueOf(object.get("servertype").getAsString());
		
		//Provider is not registered - ignore packet
		if (!registeredProviders.containsKey(key)) {
			return;
		}
		
		//Wrong ServerType - ignore packet
		if (plugin.getServerType() != type && type != ServerType.BOTH) {
			return;
		}
		//Ignore sending Server
		if (server.equals(this.server)) {
			return;
		}
		
		registeredProviders.get(key).handlePacketReceive(object);
    }

    public void sendOutgoingMessage(RedisPacketProvider provider) {
        this.sendOutgoingMessage(provider, this.server);
    }
    
    public void sendOutgoingMessage(RedisPacketProvider provider, String serverID) {
        try (Jedis jedis = this.jedisPool.getResource()) {
        	
    		JsonObject data = provider.getPacketData();

    		data.addProperty("key", provider.getID());
    		data.addProperty("server", serverID);
    		data.addProperty("servertype", provider.getReceiverType().toString());
            jedis.publish(CHANNEL, data.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    public void close() {
        this.sub.unsubscribe();
        this.jedisPool.destroy();
    }

    private static class Subscription extends JedisPubSub {
        private final RedisManager parent;

        private Subscription(RedisManager parent) {
            this.parent = parent;
        }

        @Override
        public void onMessage(String channel, String msg) {
            if (!channel.equals(CHANNEL)) {
                return;
            }
            this.parent.handleIncomingMessage(msg);
        }
    }

	
	public enum ServerType{
		BUNGEE, SPIGOT, BOTH;
	}
}
