package net.crytec.api.redis;

import com.google.gson.JsonObject;

import net.crytec.api.redis.RedisManager.ServerType;

public interface RedisPacketProvider {
	
	public String getID();
	public ServerType getReceiverType();
	public JsonObject getPacketData();
	
	public void handlePacketReceive(JsonObject data);

}
