package net.crytec.api.smartInv.content;

import java.util.Map;

import org.bukkit.entity.Player;

public interface InventoryProvider {

	void init(Player player, InventoryContents contents);

	default void update(Player player, InventoryContents contents) {
		return;
	}

	default void reOpen(Player player, InventoryContents contents) {
		if (contents.properties().isEmpty()) {
			contents.inventory().open(player, contents.pagination().getPage());
		} else {
			Map<String, Object> map = contents.properties();
			contents.inventory().open(player, contents.pagination().getPage(), map.keySet().toArray(new String[map.keySet().size()]),
					map.values().toArray(new Object[map.values().size()]));
		}
	}
	
	default void onClose(Player player, InventoryContents contents) {
		return;
	}
}