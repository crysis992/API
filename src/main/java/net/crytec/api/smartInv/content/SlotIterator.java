package net.crytec.api.smartInv.content;

import java.util.Optional;

import net.crytec.api.smartInv.ClickableItem;

public interface SlotIterator {

    enum Type {
        HORIZONTAL,
        VERTICAL
    }

    public Optional<ClickableItem> get();
    public SlotIterator set(ClickableItem item);

    public SlotIterator previous();
    public SlotIterator next();

    public SlotIterator blacklist(int row, int column);
    public SlotIterator blacklist(SlotPos slotPos);

    public int row();
    public SlotIterator row(int row);

    public int column();
    public SlotIterator column(int column);

    public boolean started();
    public boolean ended();

    public boolean doesAllowOverride();
    public SlotIterator allowOverride(boolean override);
}