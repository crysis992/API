package net.crytec.api.smartInv.content;

import java.util.Map;
import java.util.Optional;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import net.crytec.api.smartInv.ClickableItem;
import net.crytec.api.smartInv.SmartInventory;

public interface InventoryContents {

	public SmartInventory inventory();
	public Pagination pagination();
	
	public Player getHolder();

	public Optional<SlotIterator> iterator(String id);

	public SlotIterator newIterator(String id, SlotIterator.Type type, int startRow, int startColumn);
	public SlotIterator newIterator(SlotIterator.Type type, int startRow, int startColumn);

	public SlotIterator newIterator(String id, SlotIterator.Type type, SlotPos startPos);
	public SlotIterator newIterator(SlotIterator.Type type, SlotPos startPos);

	public ClickableItem[][] all();

	public Optional<SlotPos> firstEmpty();

	public Optional<ClickableItem> get(int row, int column);
	public Optional<ClickableItem> get(SlotPos slotPos);

	public InventoryContents set(int row, int column, ClickableItem item);
	public InventoryContents set(SlotPos slotPos, ClickableItem item);
	public InventoryContents set(int slot, ClickableItem item);

	public InventoryContents add(ClickableItem item);

	public InventoryContents fill(ClickableItem item);
	public InventoryContents fillRow(int row, ClickableItem item);
	public InventoryContents fillColumn(int column, ClickableItem item);
	public InventoryContents fillBorders(ClickableItem item);

	public InventoryContents fillRect(int fromRow, int fromColumn, int toRow, int toColumn, ClickableItem item);
	public InventoryContents fillRect(SlotPos fromPos, SlotPos toPos, ClickableItem item);

	public Inventory getInventory();

	public InventoryContents updateMeta(SlotPos pos, ItemMeta meta);

	public <T> T property(String name);
	public <T> T property(String name, T def);
	public Map<String, Object> properties();

	public InventoryContents setProperty(String name, Object value);
	
}