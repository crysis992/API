package net.crytec.api.smartInv.opener;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;

import com.google.common.collect.ImmutableList;

import net.crytec.api.smartInv.InventoryManager;
import net.crytec.api.smartInv.SmartInventory;

public class SpecialInventoryOpener implements InventoryOpener {

	private static final List<InventoryType> SUPPORTED = ImmutableList.of(
            InventoryType.FURNACE,
            InventoryType.WORKBENCH,
            InventoryType.DISPENSER,
            InventoryType.DROPPER,
            InventoryType.ENCHANTING,
            InventoryType.BREWING,
            InventoryType.ANVIL,
            InventoryType.BEACON,
            InventoryType.HOPPER
    );

	@Override
	public Inventory open(SmartInventory inv, Player player) {
		InventoryManager manager = InventoryManager.get();
		Inventory handle = manager.getContents(player).get().getInventory();
		
		fill(handle, manager.getContents(player).get());

		player.openInventory(handle);
		return handle;
	}

	@Override
	public boolean supports(InventoryType type) {
		return SUPPORTED.contains(type);
	}

}
