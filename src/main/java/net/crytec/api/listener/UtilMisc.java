package net.crytec.api.listener;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import net.crytec.API;
import net.crytec.api.nbt.NBTItem;

public class UtilMisc {

	protected static final String DENY_ITEM_PICKUP = "denypickup";
	protected static final String DENY_HOPPER_PICKUP = "denyhopperpickup";
	protected static final String DENY_BREAK = "denyblockbreak";
	protected static final String BLOCK_MERGE = "denyItemmerge";
	protected static final String BLOCK_PLACE = "antibuild";

	/**
	 * Deny a block from being mined at the given location.
	 * 
	 * @param location
	 */
	public static void denyBlockBreak(Location location) {
		location.getBlock().setMetadata(DENY_BREAK, new FixedMetadataValue(API.getInstance(), true));
	}

	/**
	 * Removes the lock from denyBlockBreak
	 * 
	 * @param location
	 */
	public static void allowBlockBreak(Location location) {
		location.getBlock().removeMetadata(DENY_BREAK, API.getInstance());
	}

	/**
	 * Makes an {@link Item} not being able to be picked up by entities/players
	 * or hoppers
	 * 
	 * @param item
	 */
	public static void disableItemPickup(Item item) {
		item.addScoreboardTag(DENY_HOPPER_PICKUP);
		item.setPickupDelay(Integer.MAX_VALUE);
	}

	/**
	 * Prevents an {@link Item} from being merged
	 * 
	 * @param item
	 */
	public static void disableItemMerge(Item item) {
		item.addScoreboardTag(BLOCK_MERGE);
	}

	/**
	 * Make an item not being placable by players
	 * 
	 * @param item
	 * @return returns the itemstack with the specific NBT tag
	 */
	public static ItemStack makeUnplacable(ItemStack item) {
		NBTItem nbt = new NBTItem(item);
		nbt.setByte(BLOCK_PLACE, (byte) 0);
		return nbt.getItem();
	}
}