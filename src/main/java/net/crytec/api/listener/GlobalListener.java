package net.crytec.api.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.EquipmentSlot;

import net.crytec.API;
import net.crytec.api.events.PlayerDelayedJoinEvent;
import net.crytec.api.nbt.NBTItem;

public class GlobalListener implements Listener {
	
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void improvedCooldown(PlayerInteractEvent e) {
		if (e.getHand() == EquipmentSlot.HAND) {
			if (e.getPlayer().getInventory().getItemInMainHand() != null && e.getPlayer().getInventory().getItemInMainHand().getType() != Material.AIR) {
				if (e.getPlayer().hasCooldown(e.getPlayer().getInventory().getItemInMainHand().getType())) {
					e.setCancelled(true);
					return;
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void unPlaceableItem(BlockPlaceEvent event) {
		if (event.isCancelled()) return;
		
		NBTItem nbt = new NBTItem(event.getItemInHand());
		
		if (nbt.hasKey(UtilMisc.BLOCK_PLACE)) {
			event.setBuild(false);
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPayload(BlockBreakEvent event) {
		if (event.getBlock().hasMetadata(UtilMisc.DENY_BREAK)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void disableSpigotMerge(ItemMergeEvent event) {
		if (event.getEntity() instanceof Item && event.getEntity().getScoreboardTags().contains(UtilMisc.BLOCK_MERGE)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler(ignoreCancelled = true)
	public void blockPickup (EntityPickupItemEvent e) {
		if (e.getEntity().hasMetadata(UtilMisc.DENY_ITEM_PICKUP)) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler(ignoreCancelled = true)
	public void denyHopperPickup(InventoryPickupItemEvent event) {
		if (event.getInventory().getType() != InventoryType.HOPPER ) return;

		if (event.getItem() instanceof Item && event.getItem().getScoreboardTags().contains(UtilMisc.DENY_HOPPER_PICKUP)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onEntitySpawn(CreatureSpawnEvent event) {
		if (event.isCancelled()) return;
		event.getEntity().addScoreboardTag("ctcore:spawnreason;" + event.getSpawnReason().toString());
	}
	
	@EventHandler
	public void DelayedJoin(PlayerLoginEvent e) {
		String name = e.getPlayer().getName();
		Bukkit.getScheduler().runTaskLater(API.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				Player p = Bukkit.getPlayerExact(name);
				if (p == null) { return; }
				Bukkit.getPluginManager().callEvent(new PlayerDelayedJoinEvent(p));
			}
		}, 60L);
	}
}