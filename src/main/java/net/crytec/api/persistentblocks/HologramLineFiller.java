package net.crytec.api.persistentblocks;

import java.util.function.Supplier;

import com.gmail.filoghost.holographicdisplays.api.line.TouchableLine;

public abstract class HologramLineFiller<T, L extends TouchableLine> {
	
	public HologramLineFiller(Supplier<T> valueSupp, L line) {
		this.valueSupplier = valueSupp;
		this.line = line;
	}
	
	protected final Supplier<T> valueSupplier;
	protected final L line;
	
	public abstract void updateLine();
}
