package net.crytec.api.persistentblocks;

import java.util.Map;
import java.util.function.Supplier;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

import com.google.common.collect.Maps;

import net.crytec.api.util.UtilChunk;
import net.crytec.api.util.UtilLoc;

public class PersistentBlockRegistrar {
	
	protected PersistentBlockRegistrar() {
		
		this.persistentBlockSupplier = Maps.newHashMap();
		
	}
	
	protected final Map<String, Supplier<? extends PersistentBlock>> persistentBlockSupplier;
	
	protected void registerBlock(String key, Supplier<? extends PersistentBlock> supplier) {
		this.persistentBlockSupplier.put(key, supplier);
	}
	
	protected PersistentBlock loadInstance(ConfigurationSection config, boolean chunkLoad) {
		
		String blockType = config.getString("BlockType");
		
		if(!this.persistentBlockSupplier.containsKey(blockType)) return null;
		
		Location location = UtilLoc.StringToLoc(config.getString("Location"));
		
		PersistentBlock pBlock = this.persistentBlockSupplier.get(blockType).get();
		pBlock.location = location;
		pBlock.blockType = blockType;
		
		pBlock.postInit();
		
		if(pBlock.loadData) {
			if(pBlock instanceof MultiBlock) {
				((MultiBlock)pBlock).onLoad(config);
			}else {
				pBlock.onLoad(config);
			}
		}
		
		return pBlock;
	}
	
	protected PersistentBlock createInstance(String blockType, Location location) {
		if(!this.persistentBlockSupplier.containsKey(blockType)) return null;
		PersistentBlock pBlock = this.persistentBlockSupplier.get(blockType).get();
		pBlock.location = location;
		pBlock.chunkID = UtilChunk.getChunkKey(location);
		pBlock.blockType = blockType;
		pBlock.postInit();
		return pBlock;
	}
}