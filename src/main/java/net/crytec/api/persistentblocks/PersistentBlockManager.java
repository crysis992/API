package net.crytec.api.persistentblocks;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.filoghost.holographicdisplays.api.line.TouchableLine;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import net.crytec.API;
import net.crytec.api.config.LinkedConfigurationFile;
import net.crytec.api.persistentblocks.blocks.BurnableBlock;
import net.crytec.api.persistentblocks.blocks.ExplodableBlock;
import net.crytec.api.persistentblocks.blocks.HolographicBlock;
import net.crytec.api.persistentblocks.blocks.InteractableBlock;
import net.crytec.api.persistentblocks.blocks.MovableBlock;
import net.crytec.api.persistentblocks.blocks.PistonMovableBlock;
import net.crytec.api.persistentblocks.blocks.TickableBlock;
import net.crytec.api.util.UtilChunk;
import net.crytec.api.util.UtilLoc;
/**
 * @author Gestankbratwurst, crysis992
 * There should be only one instance of this class.
 * 
 * This class manages all PersistentBlock.
 * Do not try to edit PersistentBlocks outside
 * of this instance.
 */
public class PersistentBlockManager {
	
	private static boolean INSTANCE_CHECK = false;
	private static boolean EVENTS_CHECK = false;
	
	protected static PersistentBlockManager instance;
	
	/**
	 * Not safe for instantiation.
	 * 
	 * @param plugin should be CoreAPI
	 * @param blockFolder the main folder to save all block data.
	 * 
	 */
	public PersistentBlockManager(API plugin) throws UnsupportedOperationException{
		if (INSTANCE_CHECK) {
			throw new UnsupportedOperationException("There cannot be more than 1 instance!");
		}
		
		if (!Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays")) {
			plugin.getLogger().info("No HolographicDisplays found. HolographicBlocks are not supported.");
		}
		
		instance = this;
		this.plugin = plugin;
		this.loadTracker = Maps.newHashMap();
		this.blockMap = new HashMap<Location, PersistentBlock>();
		this.blockRegistrar = new PersistentBlockRegistrar();
		PersistentBlockManager.INSTANCE_CHECK = true;
	}
	
	private final JavaPlugin plugin;
	
	private final PersistentBlockRegistrar blockRegistrar;
	
	private final HashMap<Location, PersistentBlock> blockMap;
	private final HashMap<String, HashMap<Long, HashSet<Location>>> loadTracker;
	
	private final PersistentBlockRunnable runnable = new PersistentBlockRunnable();
	private final HolographicBlockRunnable holoRunnable = new HolographicBlockRunnable();
	
	/**
	 * Safely moves a persistent block without changing life objects.
	 * Dont call on PistonEvents.
	 * 
	 * @param oldLoc the location you want it to move from.
	 * @param newLoc the location you want it to move to.
	 * @return 
	 * true if moved successfully. 
	 * false if {@link #isPersistent(Location)} returns false.
	 * false if block is not instanceof {@link MovableBlock} 
	 * or else returns {@link MovableBlock#onMove(Location, Location)}}
	 */
	
	//FIXME �berarbeiten !!
	
	public boolean moveBlock(Location oldLoc, Location newLoc) {
		if(!this.isPersistent(oldLoc)) return false;
		PersistentBlock pBlock = this.blockMap.get(oldLoc);
		if(!(pBlock instanceof MovableBlock)) return false;
		pBlock.location = newLoc;
		this.blockMap.remove(oldLoc);
		this.blockMap.put(newLoc, pBlock);
		return ((MovableBlock)pBlock).onMove(oldLoc, newLoc);
	}
	
	public boolean containsPersistentBlocks(World world) {
		return this.loadTracker.containsKey(world.getName());
	}
	
	public boolean containsPersistentBlocks(Chunk chunk) {
		String worldName = chunk.getWorld().getName();
		if(!this.loadTracker.containsKey(worldName)) return false;
		return this.loadTracker.get(worldName).containsKey(UtilChunk.getChunkKey(chunk));
	}
	
	/**
	 * Creates a new instance of {@link PersistentBlock}
	 * 
	 * @param blockKey
	 *            the blockKey. Must be provided by an instance of
	 *            {@link PersistentBlockProvider}
	 * @param location
	 *            the location at which the block should be created
	 * @param blockData
	 *            the blockData that should initially be set.
	 * @param updateState
	 *            if the {@link BlockState} of the location should be updated.
	 * @return The block if creation was successfully. null if key was not
	 *         registered by
	 *         {@link #registerPersistentBlock(PersistentBlockProvider)} null if
	 *         another persistentBlock is allready there.
	 */
	public PersistentBlock createBlock(String blockKey, Location location, BlockData blockData) {
		PersistentBlock pBlock = this.blockRegistrar.createInstance(blockKey, location);
		if(pBlock == null) return null;
		if(this.blockMap.containsKey(location)) return null;
		pBlock.blockData = blockData.getAsString();
		
		BlockState state = location.getBlock().getState();
		state.setBlockData(blockData);
		state.update(true);
		
		this.initPersistentBlock(pBlock, location);
		
		return pBlock;
	}
	
	public MultiBlock createMultiBlock(String blockKey, Location location, boolean airOnly) {
		PersistentBlock pBlock = this.blockRegistrar.createInstance(blockKey, location);
		if(pBlock == null) return null;
		if(!(pBlock instanceof MultiBlock)) return null;
		
		MultiBlock mBlock = (MultiBlock) pBlock;
		
		Predicate<Location> unsafe = (loc)->{
			
			if(loc.equals(mBlock.location)) return false;
			
			if(this.blockMap.containsKey(loc)) return true;
			
			BlockState state = loc.getBlock().getState();
			
			if(!state.getType().equals(Material.AIR) && airOnly) return true;
			
			return false;
		};
		
		if(mBlock.getBlockGrid().keySet().stream().anyMatch(unsafe)) return null;
		
		
		if(mBlock instanceof TickableBlock) this.runnable.addBlock((TickableBlock)mBlock);
		
		if (mBlock instanceof HolographicBlock) {
			if (((HolographicBlock) mBlock).updateTime() > 0) {
				this.holoRunnable.addBlock((HolographicBlock) mBlock);
			}
		}
		
		for(Map.Entry<Location, BlockData> entry : mBlock.getBlockGrid().entrySet()) {
			
			Location loc = entry.getKey();
			
			BlockState state = loc.getBlock().getState();
			
			state.setBlockData(entry.getValue());
			state.update(true);
			
			this.initPersistentBlock(mBlock, loc);
		}
		
		return mBlock;
	}
	
	public Set<PersistentBlock> getPersistentBlocksInChunk(Chunk chunk) {
		if(!this.containsPersistentBlocks(chunk)) return Sets.newHashSet();
		return this.loadTracker
		.get(chunk.getWorld().getName())
		.get(UtilChunk.getChunkKey(chunk))
		.stream().map(loc -> this.blockMap.get(loc)).collect(Collectors.toSet());
	}
	
	
	private void removeMultiBlock(MultiBlock mBlock, boolean removeBlocks) {
		
		for(Location location : mBlock.getBlockGrid().keySet()) {
			
			this.blockMap.remove(location);
			if(mBlock instanceof TickableBlock) this.runnable.removeBlock((TickableBlock)mBlock);
			if(mBlock instanceof HolographicBlock) {
				this.holoRunnable.removeBlock((HolographicBlock)mBlock);
				((HolographicBlock)mBlock).getHologram().delete();
			}
			
			if(removeBlocks) {
				BlockState state = location.getBlock().getState();
				state.setBlockData(Material.AIR.createBlockData());
				state.update(true);
			}
			
			String worldName = location.getWorld().getName();
			Long chunkID = UtilChunk.getChunkKey(location);
			
			HashMap<Long, HashSet<Location>> chunks = this.loadTracker.get(worldName);
			if(chunks == null) return;
			HashSet<Location> locations = chunks.get(chunkID);
			if(locations == null) return;
			locations.remove(location);
			if(locations.isEmpty()) chunks.remove(chunkID);
			if(chunks.isEmpty()) this.loadTracker.remove(worldName);
			
		}
		
		mBlock.onRemove();
	}
	
	public void removePersistentBlock(PersistentBlock pBlock) {
		
		if(pBlock instanceof MultiBlock) {
			this.removeMultiBlock((MultiBlock)pBlock, true);
			return;
		}
		
		Location location = pBlock.location;
		
		this.blockMap.remove(location);
		if(pBlock instanceof TickableBlock) this.runnable.removeBlock((TickableBlock)pBlock);
		if(pBlock instanceof HolographicBlock) {
			HolographicBlock hBlock = (HolographicBlock)pBlock;
			this.holoRunnable.removeBlock(hBlock);
			hBlock.getHologram().delete();
			if(!hBlock.getHologram().isDeleted()) {
				Bukkit.getScheduler().runTaskLater(plugin, () -> hBlock.getHologram().delete(), 1L);
			}
		}
		
		String worldName = location.getWorld().getName();
		Long chunkID = UtilChunk.getChunkKey(location);
		
		HashMap<Long, HashSet<Location>> chunks = this.loadTracker.get(worldName);
		if(chunks != null) {
			HashSet<Location> locations = chunks.get(chunkID);
			locations.remove(location);
			if(locations.isEmpty()) chunks.remove(chunkID);
			if(chunks.isEmpty()) this.loadTracker.remove(worldName);
		}
		
		pBlock.onRemove();
	}
	
	private void initMultiBlock(MultiBlock mBlock) {
		
		if(mBlock instanceof TickableBlock) this.runnable.addBlock((TickableBlock)mBlock);
		
		if (mBlock instanceof HolographicBlock) {
			if (((HolographicBlock) mBlock).updateTime() > 0) {
				this.holoRunnable.addBlock((HolographicBlock) mBlock);
			}
		}
		
		mBlock.getBlockGrid().keySet().forEach(loc ->{
			this.initPersistentBlock(mBlock, loc);
		});
		
	}
	
	private void initPersistentBlock(PersistentBlock pBlock, Location location) {
		
		if(this.blockMap.containsKey(location)) return;
		
		this.blockMap.put(location, pBlock);
		
		String worldName = location.getWorld().getName();
		Long chunkID = UtilChunk.getChunkKey(location);
		
		if(!this.loadTracker.containsKey(worldName)) {
			this.loadTracker.put(worldName, Maps.newHashMap());
			this.loadTracker.get(worldName).put(chunkID, Sets.newHashSet(location));
		}else if(!this.loadTracker.get(worldName).containsKey(chunkID)){
			this.loadTracker.get(worldName).put(chunkID, Sets.newHashSet(location));
		}else {
			this.loadTracker.get(worldName).get(chunkID).add(location);
		}
		
		if(!(pBlock instanceof MultiBlock)) {
			if(pBlock instanceof TickableBlock) this.runnable.addBlock((TickableBlock)pBlock);
			
			if (pBlock instanceof HolographicBlock) {
				if (((HolographicBlock) pBlock).updateTime() > 0) {
					this.holoRunnable.addBlock((HolographicBlock) pBlock);
				}
			}
		}
		
	}
	
	private void initPersistentBlock(PersistentBlock pBlock) {
		this.initPersistentBlock(pBlock, pBlock.location);
	}
	
	public void startRunnable(int repeatTime) {
		Bukkit.getScheduler().runTaskTimer(this.plugin, this.runnable, repeatTime, repeatTime);
		Bukkit.getScheduler().runTaskTimer(this.plugin, this.holoRunnable, repeatTime * 10, repeatTime * 10);
	}
	
	/**
	 * Checks if a {@link PersistentBlock} is on this location.
	 * 
	 * @param location the location to check for.
	 * @return 
	 * true if location is loaded and contains a PersistentBlock
	 * false otherwise.
	 */
	public boolean isPersistent(Location location) {
		return this.blockMap.containsKey(location);
	}
	
	public PersistentBlock getPersistentBlockOf(Location location) {
		return this.blockMap.get(location);
	}
	
//	------- Eventhandling -------	//
	
	public void registerEvents() {
		if(EVENTS_CHECK) return;
		Bukkit.getPluginManager().registerEvents(new PersistentBlockListener(this), plugin);
		EVENTS_CHECK = true;
	}
	
	protected void handleExplode(BlockExplodeEvent event) {
		
		Set<MultiBlock> explodingTracker = Sets.newHashSet();
		
		for(Block block : event.blockList()) {
			
			Location location = block.getLocation();
			
			if(this.isPersistent(location)) {
				PersistentBlock pBlock = this.getPersistentBlockOf(location);
				
				if(pBlock instanceof MultiBlock) {
					
					MultiBlock mBlock = (MultiBlock) pBlock;
					if(explodingTracker.contains(mBlock)) continue;
					explodingTracker.add(mBlock);
					
					if(!(mBlock instanceof ExplodableBlock)) {
						this.removeMultiBlock(mBlock, true);
						return;
					}
					this.removePersistentBlock(mBlock);
					((ExplodableBlock)mBlock).onExplode(event);
					if(event.isCancelled()) this.initMultiBlock(mBlock);
					
				}else {
					
					if(!(pBlock instanceof ExplodableBlock)) {
						this.removePersistentBlock(pBlock);
						return;
					}
					this.removePersistentBlock(pBlock);
					((ExplodableBlock)pBlock).onExplode(event);
					if(event.isCancelled()) this.initPersistentBlock(pBlock, location);
				}
				
			}
			
		}
		
	}
	
	protected void handleExplode(EntityExplodeEvent event) {
		
		Set<MultiBlock> explodingTracker = Sets.newHashSet();
		
		for(Block block : event.blockList()) {
			
			Location location = block.getLocation();
			
			if(this.isPersistent(location)) {
				PersistentBlock pBlock = this.getPersistentBlockOf(location);
				
				if(pBlock instanceof MultiBlock) {
					
					MultiBlock mBlock = (MultiBlock) pBlock;
					if(explodingTracker.contains(mBlock)) continue;
					explodingTracker.add(mBlock);
					
					if(!(mBlock instanceof ExplodableBlock)) {
						this.removeMultiBlock(mBlock, true);
						return;
					}
					this.removePersistentBlock(mBlock);
					((ExplodableBlock)mBlock).onExplode(event);
					if(event.isCancelled()) this.initMultiBlock(mBlock);
					
				} else {
					
					if(!(pBlock instanceof ExplodableBlock)) {
						this.removePersistentBlock(pBlock);
						return;
					}
					this.removePersistentBlock(pBlock);
					((ExplodableBlock)pBlock).onExplode(event);
					if(event.isCancelled()) this.initPersistentBlock(pBlock, location);
				}
				
			}
			
		}
		
	}
	
	protected void handleInteract(PlayerInteractEvent event) {
		if (event.getClickedBlock() == null) return;
		
		Location location = event.getClickedBlock().getLocation();
		if(this.isPersistent(location)) {
			PersistentBlock pBlock = this.getPersistentBlockOf(location);
			if(!(pBlock instanceof InteractableBlock)) return;
			if (!pBlock.blockData.equals(event.getClickedBlock().getBlockData().getAsString())) {				
				this.removePersistentBlock(pBlock);
				return;
			}
			((InteractableBlock)pBlock).onInteract(event);
		}
	}
	
	protected void handlePistonExtend(BlockPistonExtendEvent event, Block block) {
		Location location = block.getLocation();
		if(this.isPersistent(location)) {
			PersistentBlock pBlock = this.getPersistentBlockOf(location);
			if((pBlock instanceof PistonMovableBlock) && !(pBlock instanceof MultiBlock)) {
				((PistonMovableBlock)pBlock).onPistonMove(event);
				if(!event.isCancelled()) this.moveBlock(location, location.getBlock().getRelative(event.getDirection()).getLocation());
			}else {
				event.setCancelled(true);
			}
		}
	}
	
	protected void handlePistonRetract(BlockPistonRetractEvent event, Block block) {
		Location location = block.getLocation();
		if(this.isPersistent(location)) {
			PersistentBlock pBlock = this.getPersistentBlockOf(location);
			if((pBlock instanceof PistonMovableBlock) && !(pBlock instanceof MultiBlock)) {
				((PistonMovableBlock)pBlock).onPistonMove(event);
				if(!event.isCancelled()) this.moveBlock(location, location.getBlock().getRelative(event.getDirection()).getLocation());
			}else {
				event.setCancelled(true);
			}
		}
	}
	
	protected void handleBlockBurn(BlockBurnEvent event) {
		Location location = event.getBlock().getLocation();
		if(this.isPersistent(location)) {
			PersistentBlock pBlock = this.getPersistentBlockOf(location);
			if(!(pBlock instanceof BurnableBlock)) {
				this.removePersistentBlock(pBlock);
				return;
			}
			((BurnableBlock)pBlock).onBurn(event);
			if(!event.isCancelled()) this.removePersistentBlock(pBlock);
		}
	}
	
	protected void handleBlockBreak(BlockBreakEvent event) {
		Location location = event.getBlock().getLocation();
		if(this.isPersistent(location)) {
			PersistentBlock pBlock = this.getPersistentBlockOf(location);
			pBlock.onBreak(event);
			if(!event.isCancelled()) {
				this.removePersistentBlock(pBlock);
			}
		}
		
	}
	
//	------- File Handling -------	//
	
	/**
	 * Here you can register your {@link PersistentBlockProvider}
	 * The key of it will be used for handling every instance of PersistentBlock
	 * it is creating.
	 * 
	 * Changing keys will result in loss of all old instances.
	 * 
	 * @param provider the providing interface {@link PersistentBlockProvider}
	 */
	public void registerPersistentBlock(String key, Supplier<? extends PersistentBlock> supplier) {
		this.blockRegistrar.registerBlock(key, supplier);
		Bukkit.getWorlds().forEach(world ->{
			for(Chunk chunk : world.getLoadedChunks()) {
				this.loadFilesFromSystem(chunk, key);
			}
		});
	}
	
	public void saveFilesToSystem(Chunk chunk) {
		
		String worldName = chunk.getWorld().getName();
		Long chunkID = UtilChunk.getChunkKey(chunk);
		
		File chunkFolder = new File(this.plugin.getServer().getWorldContainer(), worldName + File.separator + "persistentblocks" + File.separator + chunkID);
		
		if(!this.containsPersistentBlocks(chunk)) {
			if(chunkFolder.exists()) {
				File[] typeFiles = chunkFolder.listFiles();
				for(File typeFile : typeFiles) {
					typeFile.delete();
				}
				chunkFolder.delete();
			}
			return;
		}
		
		if(!chunkFolder.exists()) chunkFolder.mkdirs();
		
		Map<String, LinkedConfigurationFile> typeConfigurations = Maps.newHashMap();
		
		Set<PersistentBlock> chunkBlocks = this.getPersistentBlocksInChunk(chunk);
		
		Set<PersistentBlock> savedMultis = Sets.newHashSet();
		
		for(PersistentBlock pBlock : chunkBlocks) {
			
			if(savedMultis.contains(pBlock)) continue;
			
			String type = pBlock.blockType;
			
			if(pBlock instanceof MultiBlock) {
				MultiBlock mBlock = (MultiBlock)pBlock;
					mBlock.getBlockGrid().keySet().forEach(loc ->{
					
					savedMultis.add(pBlock);
					this.removeMultiBlock(mBlock, false);
					
				});
			}
			
			if(!typeConfigurations.containsKey(type)) {
				typeConfigurations.put(type, new LinkedConfigurationFile(type, chunkFolder, true));
			}
			
			FileConfiguration config = typeConfigurations.get(type).getConfig();
			
			ConfigurationSection blockSection = config.createSection(UtilLoc.locToString(pBlock.location));
			
			pBlock.onUnload(blockSection);
		}
		
		typeConfigurations.values().forEach(linkedConfig -> linkedConfig.save());
	}
	
	public void loadFilesFromSystem(Chunk chunk) {
		this.blockRegistrar.persistentBlockSupplier.keySet().forEach(loaded ->{
			this.loadFilesFromSystem(chunk, loaded);
		});
	}
	
	public void loadFilesFromSystem(Chunk chunk, String blockType) {
		
		String worldName = chunk.getWorld().getName();
		Long chunkID = UtilChunk.getChunkKey(chunk);
		
		File chunkFolder = new File(this.plugin.getServer().getWorldContainer(), worldName + File.separator + "persistentblocks" + File.separator + chunkID);
		
		if(!chunkFolder.exists()) {
			return;
		}
		
		File[] typeFiles = chunkFolder.listFiles();
		
		if(typeFiles == null) {
			chunkFolder.delete();
			return;
		}
		
		File typeFile = new File(chunkFolder, blockType + ".yml");
		
		if(!typeFile.exists()) {
			return;
		}
		
		FileConfiguration config = YamlConfiguration.loadConfiguration(typeFile);
		for(String sectionPath : config.getKeys(false)) {
				
			PersistentBlock pBlock = this.blockRegistrar.loadInstance(config.getConfigurationSection(sectionPath), true);
			if(pBlock != null) {
					
				if(pBlock instanceof MultiBlock) {
					this.initMultiBlock((MultiBlock)pBlock);
				}else {
					this.initPersistentBlock(pBlock);
				}
					
			}
			
		}
		
	}
		

	
	
	
//	------- Runnables -------	//
	
	private class HolographicBlockRunnable implements Runnable {
		
		private final LinkedList<HolographicBlock> holoBlocks = Lists.newLinkedList();
		private HolographicBlock firstTicked = null;
		private long runout;
		
		protected void addBlock(HolographicBlock block) {
			this.holoBlocks.add(block);
		}
		
		protected void removeBlock(HolographicBlock block) {
			this.holoBlocks.remove(block);
		}
		
		private void updateHologram(HolographicBlock block) {
			ArrayList<HologramLineFiller<?, ? extends TouchableLine>> fillers = block.getLines();
			
			for(int index = 0; index < fillers.size(); index++) {
				fillers.get(index).updateLine();
			}
		}
		
		private boolean tickNext(boolean firstRun) {
			
			HolographicBlock next = this.holoBlocks.poll();
			
			if(firstRun) {
				this.firstTicked = next;
			}else if(next.equals(this.firstTicked)) {
				this.firstTicked = null;
				this.holoBlocks.add(next);
				return true;
			}
			
			PersistentBlock pBlock = ((PersistentBlock)next);
			pBlock.holoTicked++;
			if(pBlock.holoTicked % next.updateTime() == 0){
				this.updateHologram(next);
			};
			
			this.holoBlocks.add(next);
			return false;
		}
		
		@Override
		public void run() {
			
			if(this.holoBlocks.isEmpty()) return;
			
			this.runout = System.currentTimeMillis() + 10L;
			
			this.tickNext(true);
			
			while(System.currentTimeMillis() < this.runout) {
				if(this.tickNext(false)) break;
			}
			
		}
		
	}
	
	private class PersistentBlockRunnable implements Runnable {
		
		private final LinkedList<TickableBlock> tickableBlocks = Lists.newLinkedList();
		private TickableBlock firstTicked = null;
		private long runout;
		
		protected void addBlock(TickableBlock block) {
			this.tickableBlocks.add(block);
		}
		
		protected void removeBlock(TickableBlock block) {
			this.tickableBlocks.remove(block);
		}
		
		private boolean tickNext(boolean firstRun) {
			
			TickableBlock next = this.tickableBlocks.poll();
			
			if(firstRun) {
				this.firstTicked = next;
			}else if(next.equals(this.firstTicked)) {
				this.firstTicked = null;
				this.tickableBlocks.add(next);
				return true;
			}
			
			PersistentBlock pBlock = ((PersistentBlock)next);
			pBlock.timesTicked++;
			if(pBlock.timesTicked % next.getTicks() == 0){
				next.onTick();
			};
			
			this.tickableBlocks.add(next);
			
			return false;
		}
		
		@Override
		public void run() {
			if(this.tickableBlocks.isEmpty()) return;
			
			this.runout = System.currentTimeMillis() + 10L;
			
			this.tickNext(true);
			while(System.currentTimeMillis() < this.runout) {
				if(this.tickNext(false)) break;
			}
		}
		
	}

	
}
