package net.crytec.api.persistentblocks;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;

import lombok.Getter;
import net.crytec.API;

public class PersistentBlockAPI {

	public PersistentBlockAPI(API api, PersistentBlockManager manager) {
		this.blockManager = manager;
		this.init();
	}

	@Getter
	private PersistentBlockManager blockManager;

	private void init() {

		this.blockManager.registerEvents();
		this.blockManager.startRunnable(1);

	}

	public void shutdown() {
		
		Bukkit.getWorlds().forEach(world -> {
			
			for (Chunk chunk : world.getLoadedChunks()) {
				this.blockManager.saveFilesToSystem(chunk);
			}
			
		});
		
	}

}