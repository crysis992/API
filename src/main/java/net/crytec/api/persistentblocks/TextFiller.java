package net.crytec.api.persistentblocks;

import java.util.function.Supplier;

import com.gmail.filoghost.holographicdisplays.api.line.TextLine;

public class TextFiller extends HologramLineFiller<String, TextLine>{

	public TextFiller(Supplier<String> valueSupp, TextLine line) {
		super(valueSupp, line);
	}

	@Override
	public void updateLine() {
		this.line.setText(this.valueSupplier.get());
	}

}
