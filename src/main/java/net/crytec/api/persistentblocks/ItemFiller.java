package net.crytec.api.persistentblocks;

import java.util.function.Supplier;

import org.bukkit.inventory.ItemStack;

import com.gmail.filoghost.holographicdisplays.api.line.ItemLine;

public class ItemFiller extends HologramLineFiller<ItemStack, ItemLine>{

	public ItemFiller(Supplier<ItemStack> valueSupp, ItemLine line) {
		super(valueSupp, line);
	}

	@Override
	public void updateLine() {
		this.line.setItemStack(this.valueSupplier.get());
	}

}
