package net.crytec.api.persistentblocks;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;

import net.crytec.api.persistentblocks.blocks.InventoryBlock;
import net.crytec.api.util.UtilLoc;

public abstract class MultiBlock extends PersistentBlock{
	
	protected MultiBlock() {
		super();
	}
	
	@Override
	protected void onLoad(ConfigurationSection config) {
		
		this.getBlockGrid().forEach((loc, data) ->{
			BlockState state = loc.getBlock().getState();
			if(!state.getBlockData().equals(data)) {
				state.setBlockData(data);
				state.update(true);
			}
		});
		
		this.blockType = config.getString("BlockType");
		this.loadData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).loadInventory(config);
	}
	
	@Override
	protected void onUnload(ConfigurationSection config) {
		config.set("Location", UtilLoc.locToString(this.location));
		config.set("BlockType", blockType);
		config.set("MultiBlock", true);
		this.saveData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).saveInventory(config);
	}
	
	public abstract Map<Location, BlockData> getBlockGrid();
	public abstract void onAssemble(BlockFace direction);
	
}
