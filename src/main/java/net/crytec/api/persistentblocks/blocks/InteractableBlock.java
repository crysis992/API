package net.crytec.api.persistentblocks.blocks;

import org.bukkit.event.player.PlayerInteractEvent;

public interface InteractableBlock {
	
	public abstract void onInteract(PlayerInteractEvent event);
	
}
