package net.crytec.api.persistentblocks.blocks;

import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public interface ExplodableBlock {
	
	public void onExplode(BlockExplodeEvent event);
	public void onExplode(EntityExplodeEvent event);
	
}
