package net.crytec.api.persistentblocks.blocks;

import org.bukkit.event.block.BlockBurnEvent;

public interface BurnableBlock {
	
	public abstract void onBurn(BlockBurnEvent event);
	
}
