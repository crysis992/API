package net.crytec.api.persistentblocks.blocks;

import org.bukkit.Location;

public interface MovableBlock {
	
	public abstract boolean onMove(Location oldLoc, Location newLoc);
	
}
