package net.crytec.api.persistentblocks.blocks;

import java.util.ArrayList;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.line.TouchableLine;

import net.crytec.api.persistentblocks.HologramLineFiller;

public interface HolographicBlock {
	
	public int updateTime();
	public ArrayList<HologramLineFiller<?, ? extends TouchableLine>> getLines();
	public Hologram getHologram();
	
}