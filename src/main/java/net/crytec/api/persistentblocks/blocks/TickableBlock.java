package net.crytec.api.persistentblocks.blocks;

public interface TickableBlock {
	
	public void onTick();
	public int getTicks();
	
}
