package net.crytec.api.persistentblocks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.block.BlockBreakEvent;

import lombok.Getter;
import lombok.Setter;
import net.crytec.API;
import net.crytec.api.persistentblocks.blocks.InventoryBlock;
import net.crytec.api.util.UtilChunk;
import net.crytec.api.util.UtilLoc;

public abstract class PersistentBlock {
	
	protected PersistentBlock() {
		this.loadData = true;
		this.timesTicked = 0;
		this.holoTicked = 0;
		this.persistentBlockManager = API.getInstance().getPersistentBlocks();
	}
	
	private final PersistentBlockManager persistentBlockManager;
	
	@Getter @Setter
	protected boolean loadData;
	@Getter
	protected Location location;
	protected String blockData;
	@Getter @Setter
	protected String blockType;
	protected long chunkID;
	protected long timesTicked;
	protected long holoTicked;
	@Getter @Setter
	protected boolean stateLinked = false;
	
	public void delete() {
		this.persistentBlockManager.removePersistentBlock(this);
	}
	
	protected void onLoad(ConfigurationSection config) {
		this.stateLinked = config.getBoolean("StateLinked");
		this.blockData = config.getString("BlockData");
		this.chunkID = UtilChunk.getChunkKey(location);
		this.blockType = config.getString("BlockType");
		BlockState state = this.location.getBlock().getState();
		BlockData data = Bukkit.createBlockData(this.blockData);
		if(state.getBlockData() != data) {
			if(this.stateLinked) {
				PersistentBlockManager.instance.removePersistentBlock(this);
				return;
			}else {
				state.setBlockData(data);
				state.update(true, false);
			}
		}
		
		this.loadData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).loadInventory(config);
	}
	
	protected void onUnload(ConfigurationSection config) {
		config.set("StateLinked", this.stateLinked);
		config.set("BlockData", this.blockData);
		config.set("Location", UtilLoc.locToString(this.location));
		config.set("BlockType", blockType);
		config.set("MultiBlock", false);
		this.saveData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).saveInventory(config);
	}
	
	protected abstract void onRemove();
	protected abstract void onBreak(BlockBreakEvent event);
	protected abstract void loadData(ConfigurationSection config);
	protected abstract void saveData(ConfigurationSection config);
	protected abstract void postInit();
}