package net.crytec.api.nbt;

import java.util.logging.Logger;

import org.bukkit.inventory.ItemStack;

import net.crytec.API;
import net.crytec.api.nbt.utils.MinecraftVersion;

public class NBTTester {

	private static boolean compatible = true;

	public NBTTester() {
		this.startup();
	}

	private void startup() {
		MinecraftVersion.getVersion();
		for (ClassWrapper c : ClassWrapper.values()) {
			if (c.getClazz() == null) {
				getLogger().warning(c.name() + " did not find it's class!");
				compatible = false;
			}
		}
		for (ReflectionMethod method : ReflectionMethod.values()) {
			if (method.isCompatible() && !method.isLoaded()) {
				getLogger().warning(method.name() + " did not find the method!");
				compatible = false;
			}
		}
	}

	private Logger getLogger() {
		return API.getInstance().getLogger();
	}

	public boolean isCompatible() {
		return compatible;
	}

	public static NBTItem getNBTItem(ItemStack item) {
		return new NBTItem(item);
	}
}