package net.crytec.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.crytec.api.WGRegionEvents.MovementWay;

public class RegionLeftEvent extends RegionEvent {
	/**
	 * creates a new RegionLeftEvent
	 * 
	 * @param region   the region the player has left
	 * @param player   the player who triggered the event
	 * @param movement the type of movement how the player left the region
	 * @param parent
	 */
	public RegionLeftEvent(ProtectedRegion region, Player player, MovementWay movement, Event parent) {
		super(region, player, movement, parent);
	}
}
