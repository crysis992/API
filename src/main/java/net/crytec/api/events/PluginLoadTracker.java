package net.crytec.api.events;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;

public class PluginLoadTracker implements Listener{
	
	public PluginLoadTracker(JavaPlugin host) {
		this.plugins = Bukkit.getPluginManager().getPlugins();
		this.track = (int) Arrays.stream(plugins).filter(plugin -> plugin.isEnabled()).count();
		Bukkit.getPluginManager().registerEvents(this, host);
	}
	
	private int track;
	private final Plugin[] plugins;
	
	@EventHandler
	public void onPluginEnable(PluginEnableEvent event) {
		track++;
		if(track == plugins.length) {
			LastPluginLoadEvent loadEvent = new LastPluginLoadEvent(event.getPlugin());
			Bukkit.getPluginManager().callEvent(loadEvent);
		}
	}
	
	public static final class LastPluginLoadEvent extends Event{
		
		private static final HandlerList handlers = new HandlerList();
		
		public LastPluginLoadEvent(Plugin plugin) {
			this.plugin = plugin;
		}
		
		@Override
		public HandlerList getHandlers() {
			return handlers;
		}
		
		public static HandlerList getHandlerList() {
			return handlers;
		}
		
		@Getter
		private final Plugin plugin;
	}
}
