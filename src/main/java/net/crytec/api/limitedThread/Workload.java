package net.crytec.api.limitedThread;

public interface Workload extends Comparable<Workload>{
	
	public void process();
	public WorkloadPriority getPriority();
	
	@Override
	public default int compareTo(Workload other) {
		return this.getPriority().compareTo(other.getPriority());
	}
}
