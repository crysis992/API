package net.crytec.api.limitedThread;

public enum WorkloadPriority{
	
	/**
	 * HIGHEST zuerst; LOWEST zuletzt;
	 * Workloads mit der CRUCIAL priority werden auch
	 * bei Overhead abgearbeitet.
	 * 
	 */
	
	CRUCIAL,HIGHEST,HIGH,MEDIUM,LOW,LOWEST;
	
	public static WorkloadPriority getNextBelow(WorkloadPriority current) {
		
		switch(current) {
		case CRUCIAL: return HIGHEST;
		case HIGHEST: return HIGH;
		case HIGH: return MEDIUM;
		case MEDIUM: return LOW;
		case LOW: return LOWEST;
		case LOWEST: return null;
		default: return LOWEST;
		}
		
	}
}