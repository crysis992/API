package net.crytec.api.limitedThread;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.bukkit.Bukkit;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;

import net.crytec.API;

public abstract class LimitedThread implements Runnable{
	
	public LimitedThread(long maxMillis, long handOverDelay, int maxHandOvers) {
		this.maxMillis = maxMillis;
		this.looping = false;
		this.workloads = Maps.newHashMap();
		this.stopConditions = Sets.newHashSet();
		this.handOverDelay = handOverDelay;
		this.handOver = true;
		this.isOrigin = true;
		this.maxHandOvers = maxHandOvers;
		this.currentRun = 1;
		
		for(WorkloadPriority priority : WorkloadPriority.values()) {
			this.workloads.put(priority, Lists.newArrayList());
		}
	}
	
	public LimitedThread(long maxMillis, long handOverDelay, int maxHandOvers, Set<Workload> workloads) {
		this.maxMillis = maxMillis;
		this.looping = false;
		this.workloads = Maps.newHashMap();
		this.stopConditions = Sets.newHashSet();
		this.handOverDelay = handOverDelay;
		this.handOver = true;
		this.isOrigin = true;
		this.maxHandOvers = maxHandOvers;
		this.currentRun = 1;
		
		for(WorkloadPriority priority : WorkloadPriority.values()) {
			this.workloads.put(priority, Lists.newArrayList());
		}
		
		workloads.forEach(work->this.addWorkload(work));
	}
	
	public LimitedThread(long maxMillis, boolean looping) {
		this.maxMillis = maxMillis;
		this.looping = looping;
		this.workloads = Maps.newHashMap();
		this.stopConditions = Sets.newHashSet();
		this.handOverDelay = 1;
		this.handOver = false;
		this.isOrigin = true;
		this.maxHandOvers = 1;
		this.currentRun = 1;
		
		if(this.looping) {
			for(WorkloadPriority priority : WorkloadPriority.values()) {
				this.workloads.put(priority, Lists.newArrayList());
			}
		}
	}
	
	public LimitedThread(long maxMillis, boolean looping, Set<Workload> workloads) {
		this.maxMillis = maxMillis;
		this.looping = looping;
		this.workloads = Maps.newHashMap();
		this.stopConditions = Sets.newHashSet();
		this.handOverDelay = 1;
		this.handOver = false;
		this.isOrigin = true;
		this.maxHandOvers = 1;
		this.currentRun = 1;
		
		if(this.looping) {
			for(WorkloadPriority priority : WorkloadPriority.values()) {
				this.workloads.put(priority, Lists.newArrayList());
			}
		}
		
		workloads.forEach(work->this.addWorkload(work));
	}
	
	private LimitedThread(long maxMillis, Queue<Workload> remaining, int maxHandOvers, int runCount) {
		this.maxMillis = maxMillis;
		this.looping = false;
		this.workloads = Maps.newHashMap();
		this.stopConditions = Sets.newHashSet();
		this.handOverDelay = 1;
		this.handOver = false;
		this.isOrigin = false;
		this.workingQueue = remaining;
		this.maxHandOvers = maxHandOvers;
		this.currentRun = runCount;
	}
	
	private final long maxMillis;
	private final boolean looping;
	private final Map<WorkloadPriority, List<Workload>> workloads;
	private final Set<LimitCondition> stopConditions;
	private final long handOverDelay;
	private final boolean handOver;
	private final boolean isOrigin;
	private final int maxHandOvers;
	private final int currentRun;
	
	private WorkloadPriority currentPriority;
	private long startTime;
	private boolean running;
	
	private Queue<Workload> workingQueue;
	
	@Override
	public void run() {
		
		this.preTick();
		
		this.setup();
		this.processWork();
		
		this.postTick();
	}
	
	private void setup() {
		this.startTime = System.currentTimeMillis();
		this.running = true;
		this.currentPriority = WorkloadPriority.HIGHEST;
		if(!this.looping && this.isOrigin) {
			this.workingQueue = Queues.newPriorityQueue();
			this.workingQueue.addAll(this.workloads.get(WorkloadPriority.LOWEST));
			this.workingQueue.addAll(this.workloads.get(WorkloadPriority.LOW));
			this.workingQueue.addAll(this.workloads.get(WorkloadPriority.MEDIUM));
			this.workingQueue.addAll(this.workloads.get(WorkloadPriority.HIGH));
			this.workingQueue.addAll(this.workloads.get(WorkloadPriority.HIGHEST));
		}
	}
	
	private void processWork() {
		
		if(this.looping) {
			while(this.running) {
				this.running = this.processLoopingCycle();
			}
		}else {
			if(!this.processLinear()) {
				this.handToNextThread();
			}
		}
		
	}
	
	private boolean processLinear() {
		while(!this.workingQueue.isEmpty()) {
			if(this.isLimitReached()) return false;
			this.workingQueue.poll().process();
		}
		return true;
	}
	
	private void handToNextThread() {
		if(this.handOver && this.currentRun <= this.maxHandOvers) {
			Bukkit.getScheduler().runTaskLater(API.getInstance(), new LimitedThread(this.maxMillis, this.workingQueue, this.maxHandOvers, this.currentRun + 1) {
				@Override
				public void preTick() {
				}
				@Override
				public void postTick() {
				}}, this.handOverDelay);
		}
	}
	
	private boolean processLoopingCycle() {
		this.workloads.get(WorkloadPriority.CRUCIAL).forEach(load->{
			load.process();
		});
		this.currentPriority = WorkloadPriority.HIGHEST;
		while(this.currentPriority != null) {
			for(Workload load : this.workloads.get(this.currentPriority)) {
				if(this.isLimitReached()) return false;
				load.process();
			}
			this.nextPriority();
		}
		return true;
	}
	
	private boolean isLimitReached() {
		if(this.maxMillis <= System.currentTimeMillis() - this.startTime) return true;
		return this.stopConditions.stream().anyMatch(LimitCondition::limitReached);
	}
	
	private void nextPriority() {
		this.currentPriority = WorkloadPriority.getNextBelow(this.currentPriority);
	}
	
	public void addWorkload(Workload load) {
		this.workloads.get(load.getPriority()).add(load);
	}
	
	public void setWorkloads(List<Workload> load, WorkloadPriority priority) {
		this.workloads.put(priority, load);
	}
	
	public List<Workload> getWorkloads(WorkloadPriority priority){
		return this.workloads.get(priority);
	}
	
	public void addCondition(LimitCondition condition) {
		this.stopConditions.add(condition);
	}
	
	public abstract void preTick();
	public abstract void postTick();
}
