package net.crytec.api.limitedThread;

public interface LimitCondition {
	
	public boolean limitReached();
	
}
