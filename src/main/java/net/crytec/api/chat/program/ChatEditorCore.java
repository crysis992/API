package net.crytec.api.chat.program;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import lombok.Getter;
import me.clip.placeholderapi.PlaceholderAPI;

public class ChatEditorCore {
	
	@Getter
	private static ChatEditorCore instance;
	
	public ChatEditorCore(JavaPlugin plugin) {
		instance = this;
		Bukkit.getPluginManager().registerEvents(new InputListener(), plugin);
		this.components = Maps.newHashMap();
		this.usePlaceholder = Bukkit.getPluginManager().getPlugin("PlaceHolderAPI") != null;
		
	}
	
	private final Map<UUID, CanvasLineComponent> components;
	public final boolean usePlaceholder;
	
	public String translate(String text, OfflinePlayer player) {
		return PlaceholderAPI.setPlaceholders(player, text);
	}
	
	public CanvasLineComponent componentOf(UUID id) {
		return this.components.get(id);
	}
	
	public void registerCanvasComponent(CanvasLineComponent component) {
		this.components.put(component.getComponentID(), component);
	}
	
	public void unregisterCanvasComponent(UUID componentID) {
		this.components.remove(componentID);
	}
	
	public void unregisterCanvasComponent(CanvasLineComponent component) {
		this.unregisterCanvasComponent(component.getComponentID());
	}
}
