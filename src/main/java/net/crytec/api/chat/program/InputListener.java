package net.crytec.api.chat.program;

import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class InputListener implements Listener {
	
	private ChatEditorCore core = ChatEditorCore.getInstance();
	
	@EventHandler(ignoreCancelled = false, priority = EventPriority.MONITOR)
	public void parse(PlayerCommandPreprocessEvent event) {
		if (!event.getMessage().isEmpty() && !event.getMessage().startsWith("/ccline")) return;
		event.setCancelled(true);
		
		UUID canvasID = UUID.fromString(event.getMessage().split(" ")[1]);
		if (canvasID == null) return;
		CanvasLineComponent component = core.componentOf(canvasID);
		if (component == null) return;
		component.getConsumer().accept(event.getPlayer());
	}
}