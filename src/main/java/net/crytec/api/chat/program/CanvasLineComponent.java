package net.crytec.api.chat.program;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;

import com.google.common.collect.Lists;

import lombok.Getter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class CanvasLineComponent {
	
	private CanvasLineComponent(String string, UUID componentID, boolean clickable, Consumer<Player> consumer) {
		this.textComponent = new TextComponent(string);
		this.componentID = componentID;
		this.descriptionLines = Lists.newArrayList();
		this.clickable = clickable;
		this.consumer = consumer;
		if(clickable) {
			ChatEditorCore.getInstance().registerCanvasComponent(this);
		}
	}
	
	public CanvasLineComponent(String string, UUID componentID, Consumer<Player> consumer) {
		this(string, componentID, true, consumer);
	}
	
	public CanvasLineComponent(String string, Consumer<Player> consumer) {
		this(string, UUID.randomUUID(), consumer);
	}
	
	public CanvasLineComponent(String text) {
		this(text, UUID.randomUUID(), false, null);
	}
	
	public CanvasLineComponent(String text, UUID componentID) {
		this(text, componentID, false, null);
	}
	
	public void unregister() {
		if (this.isClickable()) {
			ChatEditorCore.getInstance().unregisterCanvasComponent(componentID);
		}
	}
	
	@Getter
	private final UUID componentID;
	@Getter
	private final boolean clickable;
	private TextComponent textComponent;
	@Getter
	private final ArrayList<String> descriptionLines;
	@Getter
	private final Consumer<Player> consumer;
	
	//test
	@Getter
	private final ArrayList<Object> data = Lists.newArrayList();
	
	private String hover = "";
	
	public CanvasLineComponent setHover(String text) {
		this.hover = text;
		return this;
	}
	
	public CanvasLineComponent setData(int index, Object data) {
		this.data.add(index, data);
		return this;
	}
	
	public TextComponent getComponent(Player player) {
		
		ComponentBuilder builder = new ComponentBuilder("");
		ChatEditorCore core = ChatEditorCore.getInstance();
		if(core.usePlaceholder) {
			this.descriptionLines.stream().map(text -> core.translate(text, player)).forEach(string -> builder.append(string));
		} else {
			this.descriptionLines.stream().forEach(string -> builder.append(string));
		}
		
		if(this.clickable) {
			this.textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ccline " + this.componentID.toString()));
			if (!hover.isEmpty()) {
				ComponentBuilder hoverBuilder = new ComponentBuilder(this.hover);
				this.textComponent.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, hoverBuilder.create() ));
			}
		}
		
		return this.textComponent;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getData(int index) {
		return (T) data.get(index);
	}
}