package net.crytec.api.chat.program;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import lombok.Getter;

public abstract class ChatProgramm {
	
	public ChatProgramm(Player player) {
		this.player = player;
		this.canvasList = Lists.newArrayList();
		this.isRunning = false;
		
	}
	
	private boolean isRunning;
	private ChatCanvas openCanvas;
	@Getter
	private final Player player;
	@Getter
	private final ArrayList<ChatCanvas> canvasList;
	
	public int registerChatCanvas(ChatCanvas canvas) {
		int index = this.canvasList.size();
		this.canvasList.add(canvas);
		return index;
	}
	
	public void changeView(int index, boolean doRefresh) {
		this.openCanvas = this.canvasList.get(index);
		if(doRefresh) {
			this.refresh();
		}
	}
	
	public void changeView(int index) {
		this.changeView(index, true);
	}
	
	public void refresh() {
		this.openCanvas.sendTo(this.player);
	}
	
	public void open(int initialWindow) {
		if(!this.isRunning) {
			this.isRunning = true;
			this.changeView(initialWindow);
			this.onOpen();
		}
	}
	
	public void close() {
		this.canvasList.forEach(canvas -> canvas.unregister());
		this.onClose();
	}
	
	public abstract void onOpen();
	public abstract void onClose();
	
}
