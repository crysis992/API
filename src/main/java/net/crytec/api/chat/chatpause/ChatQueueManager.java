package net.crytec.api.chat.chatpause;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.google.common.collect.Maps;

import net.crytec.API;

public class ChatQueueManager implements Listener {
	
	public ChatQueueManager(API plugin) {
		this.playerToListenFor = Maps.newConcurrentMap();
		this.protocolManager = ProtocolLibrary.getProtocolManager();
		this.protocolManager.addPacketListener(new ChatQueueListener(plugin, this));
		Bukkit.getPluginManager().registerEvents(this, API.getInstance());
	}
	
	private final ProtocolManager protocolManager;
	private final Map<Player, PacketJam<PacketContainer>> playerToListenFor;
	
	public boolean listenFor(Player player) {
		return this.playerToListenFor.containsKey(player);
	}
	
	public void addPacket(Player player, PacketContainer packet) throws IllegalStateException{
		if(!this.playerToListenFor.containsKey(player)) throw new IllegalStateException("Player " + player.getUniqueId() + " is not registered.");
		this.playerToListenFor.get(player).append(packet);
	}
	
	public void registerPlayer(Player player) {
		this.playerToListenFor.put(player, new PacketJam<PacketContainer>(30));
	}
	
	private void removePlayer(Player player) {
		this.playerToListenFor.remove(player);
	}
	
	public boolean isRegistered(Player player) {
		return this.playerToListenFor.containsKey(player);
	}
	
	public void releasePlayer(Player player) throws IllegalStateException{
		if(!this.playerToListenFor.containsKey(player)) throw new IllegalStateException("Player " + player.getUniqueId() + " is not registered.");
		Stream<PacketContainer> stream = this.playerToListenFor.get(player).release();
		this.removePlayer(player);
		
		//Remove player & drop packets when offline
		if (!player.isOnline()) {
			this.playerToListenFor.remove(player);
			return;
		}
		
		for(int i = 0; i < 80; i++) {
			player.sendMessage("");
		}
		
		stream.forEach(packet -> {
			try {
				this.protocolManager.sendServerPacket(player, packet);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}
	
	@EventHandler
	public void cleanOnQuit(PlayerQuitEvent event) {
		if (this.isRegistered(event.getPlayer())) {
			this.playerToListenFor.remove(event.getPlayer());
		}
	}
	
}