package net.crytec.api.chat.chatpause;

import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

public class ChatQueueListener extends PacketAdapter{

	public ChatQueueListener(Plugin plugin, ChatQueueManager queueManager) {
		super(plugin, PacketType.Play.Server.CHAT);
		this.queueManager = queueManager;
	}
	
	private final ChatQueueManager queueManager;
	
	@Override
    public void onPacketSending(PacketEvent event) {
		
		if(!this.queueManager.listenFor(event.getPlayer())) return;
		if(!event.getPacketType().equals(PacketType.Play.Server.CHAT)) return;
		if (event.getPacket().getMeta("editor").isPresent()) return;
		
		queueManager.addPacket(event.getPlayer(), event.getPacket());
		event.setCancelled(true);
    }
	
}
